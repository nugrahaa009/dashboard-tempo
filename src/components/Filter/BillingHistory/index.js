import React, { Component } from 'react'
import moment from 'moment'
import { Row, Col, Form, Button, DatePicker } from 'antd'

export default class FilterBillingHistory extends Component {

  onFinish = (values) => {
    const { onSearch } = this.props;
    values.date = moment(values.date).format('YYYY-MM-DD')
    return onSearch(values)
  }

  render() {
    return (
      <React.Fragment>
        <Row gutter={16}>
          <Form onFinish={this.onFinish} layout="inline" style={{width: '100%'}}>
            <Col sm={24} md={20}>
              <Form.Item name="date">
                <DatePicker size="large" style={{ width: '50%' }} />
              </Form.Item>
            </Col>
            <Col sm={24} md={4}>
              <Form.Item>
                <Button type="primary" size="large" htmlType="submit" block>
                  Cari
                </Button>
              </Form.Item>
            </Col>
          </Form>
        </Row>
      </React.Fragment>
    )
  }
}
