import React, { Component } from 'react'
import moment from 'moment'
import { SearchOutlined } from '@ant-design/icons'
import { Row, Col, Form, Input, DatePicker, Button } from 'antd'
const { RangePicker } = DatePicker

export default class FilterHistory extends Component {

  onFinish = (values) => {
    const { date } = values
    const { onSearch } = this.props
    values.dateFrom = date && date.length > 0 ? moment(values.date[0]).format('YYYY-MM-DD') : null
    values.dateTo = date && date.length > 0 ? moment(values.date[1]).format('YYYY-MM-DD') : null
    return onSearch(values)
  }

  render() {
    return (
      <React.Fragment>
        <Row gutter={16}>
          <Form onFinish={this.onFinish} layout="inline" style={{width: '100%'}}>
            <Col sm={24} md={10}>
              <Form.Item name="name">
                <Input 
                  style={{ height: '40px' }}
                  prefix={<SearchOutlined />}
                  placeholder="Cari Proyek"
                  size="large"
                  allowClear 
                />
              </Form.Item>
            </Col>
            <Col sm={24} md={10}>
              <Form.Item name="date">
                <RangePicker
                  placeholder={['Mulai Dari Tanggal', 'Sampai Dengan Tanggal']}
                  size="large" 
                  style={{ width: '100%' }} 
                />
              </Form.Item>
            </Col>
            <Col sm={24} md={4}>
              <Form.Item>
                <Button type="primary" size="large" htmlType="submit" block>
                  Cari
                </Button>
              </Form.Item>
            </Col>
          </Form>
        </Row>
      </React.Fragment>
    )
  }
}
