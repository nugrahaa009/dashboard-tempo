import React, { Component } from 'react'
import { Row, Col, Form, Input, Button } from 'antd'
import { SearchOutlined } from '@ant-design/icons'

export default class FilterExportExcel extends Component {

  onFinish = (e) => {
    const { onSearch } = this.props;
    return onSearch(e)
  }

  render() {
    return (
      <React.Fragment>
        <Row gutter={16}>
          <Form onFinish={this.onFinish} layout="inline" style={{width: '100%'}}>
            <Col sm={24} md={20}>
              <Form.Item name="name">
                <Input 
                  style={{ height: '40px' }}
                  prefix={<SearchOutlined />}
                  placeholder="Cari Proyek"
                  size="large"
                  allowClear 
                />
              </Form.Item>
            </Col>
            <Col sm={24} md={4}>
              <Form.Item>
                <Button type="primary" size="large" htmlType="submit" block>
                  Cari
                </Button>
              </Form.Item>
            </Col>
          </Form>
        </Row>
      </React.Fragment>
    )
  }
}
