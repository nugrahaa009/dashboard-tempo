import React, { Component } from 'react'
import { Row, Col, Button, Form, Input, Select } from 'antd'
import { SearchOutlined } from '@ant-design/icons'

export default class Filter extends Component {

  onFinish = (e) => {
    const { onSearch } = this.props;
    return onSearch(e)
  }

  render() {
    const { fase1, fase2, fase3 } = this.props;

    return (
      <Row gutter={16}>
        <Form onFinish={this.onFinish} layout="inline" style={{ width: '100%' }}>
          <Col xs={24} md={{ span: fase3 ? 6 : 7 }}>
            <Form.Item name="search" initialValue="">
              <Input 
                prefix={<SearchOutlined />}
                placeholder={fase1 ? `Search ${fase1.map((res) => res)}` : 'Cari' }
                size="large"
                allowClear 
              />
            </Form.Item>
          </Col>

          <Col xs={24} md={{ span: fase3 ? 5 : 7 }}>
            <Form.Item name={fase2.key.toLowerCase()} initialValue={fase2.value[0].value}>
              <Select
                size="large"
                style={{ width: '100%' }}
                placeholder={fase2.key}
              >
                {fase2.value.map((res, i) => <Select.Option key={i.toString()} value={res.value}>{res.name}</Select.Option>)}
              </Select>
            </Form.Item>
          </Col>

          {
            fase3 &&
            <Col xs={24} md={{ span: fase3 ? 5 : 7 }}>
              <Form.Item name="userType" initialValue={0}>
                <Select
                  size="large"
                  style={{ width: '100%' }}
                  placeholder={fase2.key}
                >
                  <Select.Option value={0}>Semua Peran</Select.Option>
                  <Select.Option value={2}>Admin</Select.Option>
                  <Select.Option value={3}>Tukang</Select.Option>
                  <Select.Option value={4}>Stakeholder</Select.Option>
                  <Select.Option value={5}>Quality Control</Select.Option>
                  <Select.Option value={6}>Fitting</Select.Option>
                </Select>
              </Form.Item>
            </Col>
          }

          <Col xs={24} md={{ span: fase3 ? 5 : 7 }}>
            <Form.Item name="orderBy" initialValue="desc">
              <Select
                size="large"
                style={{ width: '100%' }}
                placeholder="Urutan Dari"
              >
                <Select.Option value="asc">Tanggal Terakhir</Select.Option>
                <Select.Option value="desc">Tanggal Terbaru</Select.Option>
              </Select>
            </Form.Item>
          </Col>

          <Col xs={24} md={{ span: 3 }}>
            <Form.Item>
              <Button type="primary" size="large" htmlType="submit" block>
                Cari
              </Button>
            </Form.Item>
          </Col>
        </Form>
      </Row>
    )
  }
}
