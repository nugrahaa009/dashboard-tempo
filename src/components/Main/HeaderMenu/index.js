import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Layout, Menu, Dropdown, Space, Typography, Row, Col } from 'antd'
import { DownOutlined, LogoutOutlined, EditOutlined } from '@ant-design/icons'

const { Header } = Layout

const { Text } = Typography

export default class HeaderMenu extends Component {
  render() {
    const { dataUser, logout } = this.props

    const menu = (
      <Menu>
        {
          dataUser.userType === 1 ?
          <Menu.Item icon={<EditOutlined />}>
            <Link to="/profile/edit">Ubah Profil</Link>
          </Menu.Item>
          :
          null
        }
        {
          dataUser.userType === 2 ?
          <Menu.Item icon={<EditOutlined />}>
            <Link to="/admin/profile/edit">Ubah Profil</Link>
          </Menu.Item>
          :
          null
        }
        {
          dataUser.userType === 4 ?
          <Menu.Item icon={<EditOutlined />}>
            <Link to="/stakeholder/profile/edit">Ubah Profil</Link>
          </Menu.Item>
          :
          null
        }
        <Menu.Item icon={<LogoutOutlined />}>
          <Link to="#" onClick={() => logout()}>Logout</Link>
        </Menu.Item>
      </Menu>
    );

    return (
      <React.Fragment>
        <Header className="site-layout-sub-header-background" style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
          <Row gutter={16}>
            <Col sm={24} md={24} style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Col className="logo">
                <img src={require('../../../assets/img/logo.png')} alt="" style={{ width: '40%' }} />
              </Col>
              <Dropdown overlay={menu}>
                <Link to="#">
                  <Space>
                    <Text style={{ color: 'white' }}>
                      {dataUser.userData.data.role.name}
                    </Text>
                    <DownOutlined style={{ color: 'white' }} />
                  </Space>
                </Link>
              </Dropdown>
            </Col>
          </Row>
        </Header>
      </React.Fragment>
    )
  }
}
