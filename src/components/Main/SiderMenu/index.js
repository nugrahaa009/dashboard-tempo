import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Layout, Menu, Typography } from 'antd'
import { AreaChartOutlined, PieChartOutlined, DollarOutlined, LaptopOutlined, SettingOutlined, UserOutlined, DatabaseOutlined } from '@ant-design/icons'

const { Sider } = Layout

const { Text } = Typography

export default class SiderMenu extends Component {

  render() {
    const { permission, index } = this.props
    
    return (
      <React.Fragment>
        <Sider style={{ height: '100vh', position: 'fixed', left: 0, paddingTop: 63 }}>
          <Menu defaultSelectedKeys={[`${index}`]} defaultOpenKeys={[`sub${index}`]} mode="inline" style={{ height: '100%', borderRight: 0 }}>
            {
              permission.map((response, i) => {
                return (
                  response.show === true ?
                  <Menu.Item key={response.index}>
                    {response.title === "Dashboard" ? <PieChartOutlined style={{ fontWeight: 'bold', fontSize: 15 }} /> : null}
                    {response.title === "Pengguna"      ? <UserOutlined style={{ fontWeight: 'bold', fontSize: 15 }} />   : null}
                    {response.title === "Pembayaran"   ? <DollarOutlined style={{ fontWeight: 'bold', fontSize: 15 }} />   : null}
                    {response.title === "Proyek"   ? <LaptopOutlined style={{ fontWeight: 'bold', fontSize: 15 }} />   : null}
                    {response.title === "Report"    ? <AreaChartOutlined style={{ fontWeight: 'bold', fontSize: 15 }} />   : null}
                    {response.title === "Pengaturan"   ? <SettingOutlined style={{ fontWeight: 'bold', fontSize: 15 }} />   : null}
                    {response.title === "Admin"     ? <DatabaseOutlined style={{ fontWeight: 'bold', fontSize: 15 }} />   : null}

                    <Link to={response.path}>
                      <Text>{response.title}</Text>
                    </Link>
                  </Menu.Item>
                  :
                  null
                )
              })
            }
          </Menu>
        </Sider>
      </React.Fragment>
    )
  }
}
