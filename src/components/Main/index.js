import React, { Component } from 'react'
import { connect } from 'react-redux'
import { checkAuth, setLogout } from '../../redux/actions/auth/authAction'
import { Layout, Modal } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import HeaderMenu from './HeaderMenu'
import SiderMenu from './SiderMenu'
import ContentMenu from './ContentMenu'

const { confirm } = Modal

class Main extends Component {
  constructor(props) {
    super(props)

    this.state = {
      _newMenu: null
    }
  }

  componentDidMount() {
    const { permission } = this.props;
    var array = [];
    permission.forEach(res => {
      if (res.show) {
        var hh = {
          key: res.path.replace('/', ''),
          title: res.title
        }
        array.push(hh);
      }
    });
    return this.setState({ _newMenu: array })
  }

  logout(){
    const { actionSetLogout, history } = this.props;
    confirm({
      title: 'Apa anda yakin ingin keluar?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Keluar',
      okType: 'danger',
      cancelText: 'Cancel',
      onOk: async () => {
        await actionSetLogout(null, () => history.push('/login'))
      },
    });
  }

  render() {
    const { dataUser, children, permission, index, private: privatePage } = this.props

    return (
      <Layout style={{ margin: 0, padding: 0 }}>
        {
          privatePage &&
          <React.Fragment>
            <Layout>
              <HeaderMenu logout={() => this.logout()} dataUser={dataUser} />
            </Layout>
            <Layout className="site-layout" style={{ marginLeft: 200 }}>
              <SiderMenu permission={permission} index={index} />
              <ContentMenu children={children}/>
            </Layout>
          </React.Fragment>
        }
      </Layout>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    permission: state.auth.permission,
    dataUser: state.auth,
  }
}

const mapActionsToProps = {
  actioncheckAuth: checkAuth,
  actionSetLogout: setLogout
}
export default connect(mapStateToProps, mapActionsToProps)(Main)
