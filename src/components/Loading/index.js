import React, { Component } from 'react'
import { Spin } from 'antd'

export default class Loading extends Component {
  render() {
    const { height } = this.props;
    return (
      <div className="loading" style={{ height: height || '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <Spin size="large" />
      </div>
    )
  }
}