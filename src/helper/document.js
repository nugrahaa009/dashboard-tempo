const getBase64 = (file, cb) => {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    cb(reader.result);
  };
  reader.onerror = function (error) {
    ('Error: ', error);
  };
}

export const upload = (file, successCB, failedCB) =>{
  return getBase64(file, (result) => {
    const bodyData = { 
      size: file.size,
      name: file.name, 
      type: file.type, 
      content: result 
    }; 
    return successCB(bodyData)
  });
}

export const documentView = (content, type, successCB) => {
  var resData;  
  const blob = b64toBlob(content, type);
  const blobUrl = URL.createObjectURL(blob);
  resData = blobUrl;
  return successCB && successCB(resData)
}

const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);
    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

