import SUPER_ADMIN from './SUPER_ADMIN.json'
import ADMIN from './ADMIN.json'
import STAKEHOLDER from './STAKEHOLDER.json'

export {
  SUPER_ADMIN,
  ADMIN,
  STAKEHOLDER
}