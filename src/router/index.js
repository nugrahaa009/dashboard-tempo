import React, { Component } from 'react'
import '../assets/styles/global.css'
import * as Containers from '../containers'
import { Loading } from '../components'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { PrivateRoute, AuthRoute } from './HandleRoute'
import { checkAuth } from '../redux/actions/auth/authAction'
import { convertMenu } from '../helper/menuSelection'

export class Route extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       _menu: []
    }
  }
  
  componentDidMount() {
    const { actionCheckAuth } = this.props
    return actionCheckAuth()
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    const { permission, loading, userType } = nextProps
    !loading && convertMenu(permission, userType, (_res) => {
      this.setState({
        _menu: _res,
      })
    })
  }

  render() {
    const { loading } = this.props
    const { _menu } = this.state

    if(loading){
      return <Loading />
    }
    
    return (
      <React.Fragment>
        <Router>
          <Switch>
            <AuthRoute exact path='/login' component={Containers.Login} passProps={this.props} private={false} />
            {
              _menu.map((response, i) => {
                return <PrivateRoute {...response} key={i} exact component={Containers[response.component]} passProps={this.props} />
              })
            }
            <PrivateRoute exact path='*' index={404} component={Containers.NotFound} passProps={this.props} />
          </Switch>
        </Router>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  loading     : state.auth.loading,
  authed      : state.auth.authed,
  permission  : state.auth.permission,
  userType    : state.auth.userType,
})

const mapDispatchToProps = {
  actionCheckAuth: checkAuth
}

export default connect(mapStateToProps, mapDispatchToProps)(Route)