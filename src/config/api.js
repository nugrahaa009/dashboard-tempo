import axios from 'axios';
import { env } from './index';
import Cookie from 'js-cookie';
import { Base64 } from 'js-base64';

export const GET = (path, payload) => {
  var getToken = Cookie.get('user');
  var header = {
    "Authorization": `Bearer ${getToken}`,
    "Content-Type": "application/json",
    "data": Base64.encode(JSON.stringify(payload))
  }
  return new Promise((resolve ,reject) => {
    axios.get(`${env.API}${path}`, {
      headers: header,
      onUploadProgress: (progressEvent) => {
        var percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
        console.log(999, percentCompleted, 'progressEvent');
        // return progress && progress(percentCompleted)
      },
    }).then((response) => {
      let data = response.data;
      if(data.status.code === 200){
        return resolve(data);
      }else{
        var _err = { message: 'error' }
        return reject(_err)
      }
    }).catch((err) => {

      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })
}


export const POST  = (path, payload) => {
  var getToken = Cookie.get('user');
  var header = {
    "Authorization": `Bearer ${getToken}`,
    "Content-Type": "application/json",
  }
  return new Promise((resolve ,reject) => {
    axios.post(`${env.API}${path}`,payload , {
      headers: header,
      onUploadProgress: (progressEvent) => {
        var percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
        console.log(999, percentCompleted, 'progressEvent');
        // return progress && progress(percentCompleted)
      },
    }).then((response) => {
      let data = response.data;
      if(data.status.code === 200){
        return resolve(data);
      }else{
        var _err = { message: data.status.message }
        return reject(_err);
      }
    }).catch((err) => {
      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })
}

export const UPLOAD = async (path, payload) => {
  var getToken = Cookie.get('user');
  return new Promise((resolve, reject) => {
    var data = new FormData();
    for (const [val, name] of Object.entries(payload)) {
      data.append(val, name);
    } 
    axios.post(`${env.API}${path}`, data, {
      headers: {
        "Accept": 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "multipart/form-data",
        "Authorization": `Bearer ${getToken}`,
      },
    }).then((response) => {
      let data = response.data;
      if(data.status.code === 200){
        return resolve(data);
      }else{
        var _err = { message: data.status.message }
        return reject(_err);
      }
    }).catch((err) => {
      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })   
}
