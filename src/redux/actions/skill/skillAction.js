import { API } from '../../../config'
import { errorHandler } from '../auth/errorAction'

export const unmountDataSkill = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_DATA_SKILL' })
}

export const unmountDetailSkill = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_DETAIL_SKILL' })
}

export const listSkill = (page, pageSize, filter ) => async dispatch => {
  console.log(222, filter);
  await dispatch({ type: 'LOAD_DATA_SKILL' })
  const payload = {
    current: parseFloat(page),
    pageSize: pageSize,
    filter: filter
  }
  return API.GET('/skill', payload).then((response) => {
    const body = response.body
    dispatch({
      type: 'LOAD_DATA_SKILL_SUCCESS', 
      payload: {
        data: body.data,
        pagination: {
          current: body.page,
          pageSize: body.perPage,
          total: body.total
        }
    }})
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LOAD_DATA_SKILL_FAILED' }), 
    ))
  })
}

export const detailSkill = (id, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOAD_SKILL_DETAIL'})
  const payload = { id: id }
  return API.GET('/skill/detail', payload).then((response) => {
    const data = response.body;
    dispatch({ type: 'LOAD_SKILL_DETAIL_SUCCESS', payload : { uid: response.uid, data: data } })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(err), dispatch({ type: 'LOAD_SKILL_DETAIL_FAILED' }) ), 
    ))
  })
}

export const editSkill = (value, successCB, failedCB) => async dispatch => {
  API.POST('/skill/edit', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const createSkill = (value, successCB, failedCB) => async dispatch => {
  API.POST('/skill/create', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const deleteSkill = (value, successCB, failedCB) => async dispatch => {
  API.POST('/skill/delete', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}
