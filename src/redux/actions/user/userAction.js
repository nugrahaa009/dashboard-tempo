import { API } from '../../../config'
import { errorHandler } from '../auth/errorAction'

export const unmountDataUser = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DATA_USER'})
}

export const unmountDetailUser = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DETAIL_USER'})
}

export const listUser = (page, userType, pageSize, filter ) => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_USER' })
  const payload = {
    current: parseFloat(page),
    pageSize: pageSize,
    userType: userType,
    filter: filter
  }
  return API.GET('/users', payload).then((response) => {
    const body = response.body
    dispatch({
      type: 'LOAD_DATA_USER_SUCCESS', 
      payload: {
        data: body.data,
        pagination: {
          current: body.page,
          pageSize: body.perPage,
          total: body.total
        }
    }})
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LOAD_DATA_USER_FAILED' }), 
    ))
  })
}

export const detailUser = (id, page, pageSize, filter, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOAD_USER_DETAIL'})
  const payload = { 
    uid: id,
    current: parseFloat(page),
    pageSize: pageSize,
    filter: filter
  }
  API.GET('/users/detail', payload).then((response) => {
    const body = response.body
    dispatch({ 
      type: 'LOAD_USER_DETAIL_SUCCESS', 
      payload : { 
        uid: response.uid, 
        data: body,
        pagination:{
          current: body.listPayment.page,
          total: body.listPayment.total,
          pageSize: body.listPayment.perPage
        }
      } 
    })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(err), dispatch({ type: 'LOAD_USER_DETAIL_FAILED' }) ), 
    ))
  })
}

export const createUser = (value, successCB, failedCB) => async dispatch => {
  API.POST('/users/create', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const editUser = (value, successCB, failedCB) => async dispatch => {
  API.POST('/users/edit', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const deleteUser = (value, successCB, failedCB) => async dispatch => {
  API.POST('/users/delete', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}