import { API } from '../../../config'
import { errorHandler } from '../auth/errorAction'

export const unmountDataDashboard = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_DASHBOARD'})
}

export const dashboardData = (filter, successCB, failedCB) => async dispatch => {
  const payload = {
    filter: filter
  }
  API.GET('/dashboard', payload).then((response) => {
    const data = response.body;
    dispatch({ type: 'LOAD_DASHBOARD_SUCCESS', payload : { data: data } })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(err), dispatch({ type: 'LOAD_DASHBOARD_FAILED' }) ), 
    ))
  })
}
