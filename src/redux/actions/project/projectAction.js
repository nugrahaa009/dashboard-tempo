import { API } from '../../../config'
import { errorHandler } from '../auth/errorAction'

export const unmountDataProject = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DATA_PROJECT'})
}

export const unmountDetailProject = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_PROJECT_DETAIL'})
}

export const listProject = (id, page, pageSize, filter ) => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_PROJECT' })
  const payload = {
    uid: id,
    current: parseFloat(page),
    pageSize: pageSize,
    filter: filter
  }
  return API.GET('/project', payload).then((response) => {
    const body = response.body
    dispatch({
      type: 'LOAD_DATA_PROJECT_SUCCESS', 
      payload: {
        data: body.data,
        pagination: {
          current: body.page,
          pageSize: body.perPage,
          total: body.total,
        }
    }})
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LOAD_DATA_PROJECT_FAILED' }), 
    ))
  })
}

export const detailProject = (id, type, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOAD_PROJECT_DETAIL'})
  const payload = { 
    uid: id,
    type: type
  }
  API.GET('/project/detail', payload).then((response) => {
    const data = response.body;
    dispatch({ type: 'LOAD_PROJECT_DETAIL_SUCCESS', payload : { uid: response.uid, data: data } })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(err), dispatch({ type: 'LOAD_PROJECT_DETAIL_FAILED' }) ), 
    ))
  })
}

export const importCSV = (value, successCB, failedCB) => async dispatch => {
  API.UPLOAD('/document', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const printLoketing = (value, successCB, failedCB) => async dispatch => {
  API.POST('/project/print', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const createProject = (value, successCB, failedCB) => async dispatch => {
  API.UPLOAD('/project/create', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const notesProject = (value, successCB, failedCB) => async dispatch => {
  API.POST('/project/notes/create', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const editProject = (value, successCB, failedCB) => async dispatch => {
  API.POST('/project/edit', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const deleteListProject = (value, successCB, failedCB) => async dispatch => {
  API.POST('/project/delete', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const deleteProject = (value, successCB, failedCB) => async dispatch => {
  API.POST('/plan/project/delete', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const holdProject = (value, successCB, failedCB) => async dispatch => {
  API.POST('/project/change-status', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}