import { removedata, refreshToken } from './authAction';

export const errorHandler = (status, cb) => async dispatch => {
  if(status){
    switch (status.shortCode) {
      case "SESSION_EXPIRED":
        return dispatch(refreshToken())
      case "INVALID_TOKEN 2":
        await dispatch({type: 'SET_LOGOUT'});
        return dispatch(removedata(() => {
          return dispatch({type: 'SET_LOGOUT_SUCCESS'});
        }))
      case "Error":
      case "UNAUTHORIZED":
        return null     
      case "FAILED":
        return null  
      default:
        return cb
    }
  }else{
    await dispatch({type: 'SET_LOGOUT'});
    return dispatch(removedata(() => {
      return dispatch({type: 'SET_LOGOUT_SUCCESS'});
    }))
  }
}