import jwt from 'jsonwebtoken'
import Cookie from "js-cookie"
import { API } from '../../../config'
import { errorHandler } from './errorAction'
import { SUPER_ADMIN, ADMIN, STAKEHOLDER} from '../../../permissions'

export const checkAuth = () => async dispatch => {
  await dispatch({ type: 'LOAD_AUTH' })
  const getToken = Cookie.get('user')
  if(getToken){
    const userData = jwt.decode(getToken)
    const permission = getPermission(userData.data.userType)
    if(getToken !== null){
      return dispatch({ 
        type: 'LOAD_AUTH_SUCCESS', 
        payload: { 
          userData: userData, 
          userType: userData.data.userType,
          permission: permission
        } 
      })
    }else {
      return dispatch(setLogout());
    }
  }else{
    return dispatch({ type: 'LOAD_AUTH_FAILED' })
  }
}

export const setLogin = (value, successCB, failedCB) => async dispatch => {
  await dispatch({ type: 'LOAD_AUTH' })
  API.POST('/login', value).then((response) => {
    const body = response.body
    const token = body.token
    const refreshToken = body.refreshToken
    const userData = jwt.decode(token)
    const permission = getPermission(userData.data.userType)
    Cookie.set('user', token)
    Cookie.set('userRefreshTkn', refreshToken)
    dispatch({ type: 'LOAD_AUTH_SUCCESS', payload: { 
      userData: userData,
      userType: userData.data.userType,
      permission: permission
    } 
  })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      failedCB && failedCB(err), 
      dispatch({ type: 'LOAD_AUTH_FAILED' }) ), 
    )
  })
}

export const setLogout = (successCB, failedCB) => async dispatch => {
  await dispatch({type: 'SET_LOGOUT'})
  return API.GET('/logout', null).then((response) => {
    return dispatch(removedata(() => {
      dispatch({ type: 'SET_LOGOUT_SUCCESS'})
      return successCB && successCB();
    }))
  }).catch((err) => {
    return (
      failedCB && failedCB(),
      dispatch(errorHandler(
        err, 
        dispatch({ type: 'SET_LOGOUT_FAILED'})
      ))
    )
  })
}

export const checkEmail = (value, successCB, failedCB) => () => {
  API.POST('/check/email', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return failedCB && failedCB(err)
  })
}

export const getPermission = (type) => {
  switch (type) {
    case 1:
      return SUPER_ADMIN
    case 2:
      return ADMIN
    case 4:
      return STAKEHOLDER
    default:
      return null
  }
}

export const refreshToken = () => async dispatch => {
  await dispatch({type: 'LOAD_AUTH'})
  const refreshToken = Cookie.get('userRefreshTkn')
  const payload = { 
    "refreshToken": refreshToken 
  }
  return API.POST('/refresh/token', payload).then((response) => {
    const body = response.body
    const token = body.token
    const refreshToken = body.refreshToken
    Cookie.set('user', token);
    Cookie.set('userRefreshTkn', refreshToken);
    const userData = jwt.decode(token)
    const permission = getPermission(userData.data.userType)
    return dispatch({ 
      type: 'LOAD_AUTH_SUCCESS', 
      payload: { 
        userData: userData, 
        permission: permission, 
        userType: userData.data.userType
      } 
    })
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LOAD_AUTH_FAILED' }), 
    ))
  })
}

export const removedata = (cb) => async (dispatch, getState) => {
  await Cookie.remove('user')
  await Cookie.remove('userRefreshTkn')
  return cb()
}