import { API } from '../../../config'
import { errorHandler } from '../auth/errorAction'

export const unmountDataPayment = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DATA_PAYMENT'})
}

export const unmountDetailPayment = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DETAIL_PAYMENT'})
}

export const unmountDataHistory = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DATA_INVOICE'})
}

export const unmountDetailHistory = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DETAIL_INVOICE'})
}

export const listPayment = (page, pageSize, filter ) => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_PAYMENT' })
  const payload = {
    current: parseFloat(page),
    pageSize: pageSize,
    filter: filter
  }
  return API.GET('/payment', payload).then((response) => {
    const body = response.body
    dispatch({
      type: 'LOAD_DATA_PAYMENT_SUCCESS', 
      payload: {
        data: body.data,
        pagination: {
          current: body.page,
          pageSize: body.perPage,
          total: body.total
        }
    }})
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LOAD_DATA_PAYMENT_FAILED' }), 
    ))
  })
}

export const listHistory = (page, pageSize, filter) => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_INVOICE' })
  const payload = {
    current: parseFloat(page),
    pageSize: pageSize,
    filter: filter
  }
  return API.GET('/invoice', payload).then((response) => {
    const body = response.body
    dispatch({
      type: 'LOAD_DATA_INVOICE_SUCCESS', 
      payload: {
        data: body.data,
        pagination: {
          current: body.page,
          pageSize: body.perPage,
          total: body.total
        }
    }})
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LOAD_DATA_INVOICE_FAILED' }), 
    ))
  })
}

export const detailPayment = (id, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOAD_PAYMENT_DETAIL'})
  const payload = { uid: id }
  API.GET('/payment/detail', payload).then((response) => {
    const data = response.body;
    dispatch({ type: 'LOAD_PAYMENT_DETAIL_SUCCESS', payload : { uid: response.uid, data: data } })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(err), dispatch({ type: 'LOAD_PAYMENT_DETAIL_FAILED' }) ), 
    ))
  })
}

export const detailHistory = (id, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOAD_INVOICE_DETAIL'})
  const payload = { invoiceNumber: id }
  API.GET('/invoice/detail', payload).then((response) => {
    const data = response.body;
    dispatch({ type: 'LOAD_INVOICE_DETAIL_SUCCESS', payload : { uid: response.uid, data: data } })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(err), dispatch({ type: 'LOAD_INVOICE_DETAIL_FAILED' }) ), 
    ))
  })
}

export const documentPayment = (value, successCB, failedCB) => async dispatch => {
  API.POST('/invoice', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const exportExcel = (value, successCB, failedCB) => async dispatch => {
  API.POST('/export', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}
