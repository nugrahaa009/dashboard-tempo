import { API } from '../../../config'
import { errorHandler } from '../auth/errorAction'

export const unmountDataPlanning = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DATA_PLANNING'})
}

export const unmountDetailPlanning = () => (dispatch) => {
  return dispatch({type: 'UNMOUNT_DETAIL_PLANNING'})
}

export const listPlanning = (page, pageSize, filter ) => async dispatch => {
  await dispatch({ type: 'LOAD_DATA_PLANNING' })
  const payload = {
    current: parseFloat(page),
    pageSize: pageSize,
    filter: filter
  }
  return API.GET('/plan/project', payload).then((response) => {
    const body = response.body
    dispatch({
      type: 'LOAD_DATA_PLANNING_SUCCESS', 
      payload: {
        data: body.data,
        pagination: {
          current: body.page,
          pageSize: body.perPage,
          total: body.total
        }
    }})
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LOAD_DATA_PLANNING_FAILED' }), 
    ))
  })
}

export const detailPlanning = (id, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOAD_PLANNING_DETAIL'})
  const payload = { uid: id }
  API.GET('/plan/project/detail', payload).then((response) => {
    const data = response.body;
    dispatch({ type: 'LOAD_PLANNING_DETAIL_SUCCESS', payload : { uid: response.uid, data: data } })
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(err), dispatch({ type: 'LOAD_PLANNING_DETAIL_FAILED' }) ), 
    ))
  })
}

export const editPlanning = (value, successCB, failedCB) => async dispatch => {
  API.UPLOAD('/plan/project/edit', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const holdPlanning = (value, successCB, failedCB) => async dispatch => {
  API.POST('/project/change-status', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}

export const printPlanning = (value, successCB, failedCB) => async dispatch => {
  API.POST('/project/qr', value).then((response) => {
    return successCB && successCB(response)
  }).catch((err) => {
    return dispatch(errorHandler(
      err,
      ( failedCB && failedCB(err) )
    ))
  })
}