import { API } from '../../../config'
import { errorHandler } from '../auth/errorAction'

export const unmountDataMaster = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_DATA_MASTER' })
}

export const listDataMaster = (type, successCB, failedCB) => async dispatch => {
  await dispatch({ "type": 'LIST_DATA_MASTER' });
  const payload = { "type": type }
  return API.GET('/master', payload).then((res) => {
    const body = res.body;
    dispatch({ "type": 'LIST_DATA_MASTER_SUCCESS', payload:{ 
      data: body,
    }});
    return successCB && successCB()
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      dispatch({ type: 'LIST_DATA_MASTER_FAILED' }), 
    ))
  })
}
