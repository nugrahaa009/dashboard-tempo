const LOAD_INVOICE_DETAIL         = 'LOAD_INVOICE_DETAIL'
const LOAD_INVOICE_DETAIL_SUCCESS = 'LOAD_INVOICE_DETAIL_SUCCESS'
const LOAD_INVOICE_DETAIL_FAILED  = 'LOAD_INVOICE_DETAIL_FAILED'
const UNMOUNT_DETAIL_INVOICE      = 'UNMOUNT_DETAIL_INVOICE'

const initialState = {
  data: null,
  loading: true,
  message: null
}

export default (state = initialState, action = {}) => {
  switch(action.type){
    case LOAD_INVOICE_DETAIL:
      return{
        ...state,
        loading: true
      }
    case LOAD_INVOICE_DETAIL_SUCCESS:
      return{ 
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS'
      }
    case LOAD_INVOICE_DETAIL_FAILED:
      return{ 
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED'
      }
      case UNMOUNT_DETAIL_INVOICE:
        return{ 
          ...state, 
          loading: true,
          data: null,
        }
    default:
      return state
  }
}