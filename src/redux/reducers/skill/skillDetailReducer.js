const LOAD_SKILL_DETAIL         = 'LOAD_SKILL_DETAIL'
const LOAD_SKILL_DETAIL_SUCCESS = 'LOAD_SKILL_DETAIL_SUCCESS'
const LOAD_SKILL_DETAIL_FAILED  = 'LOAD_SKILL_DETAIL_FAILED'
const UNMOUNT_DETAIL_SKILL      = 'UNMOUNT_DETAIL_SKILL'

const initialState = {
  data: null,
  loading: true,
  message: null
}

export default (state = initialState, action = {}) => {
  switch(action.type){
    case LOAD_SKILL_DETAIL:
      return{
        ...state,
        loading: true
      }
    case LOAD_SKILL_DETAIL_SUCCESS:
      return{ 
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS'
      }
    case LOAD_SKILL_DETAIL_FAILED:
      return{ 
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED'
      }
      case UNMOUNT_DETAIL_SKILL:
        return{ 
          ...state, 
          loading: true,
          data: null,
        }
    default:
      return state
  }
}