const LOAD_DATA_PROJECT         = 'LOAD_DATA_PROJECT'
const LOAD_DATA_PROJECT_SUCCESS = 'LOAD_DATA_PROJECT_SUCCESS'
const LOAD_DATA_PROJECT_FAILED  = 'LOAD_DATA_PROJECT_FAILED'
const UNMOUNT_DATA_PROJECT      = 'UNMOUNT_DATA_PROJECT'

const initialState = {
  loading: true,
  data: null,
  message: null,
  pagination: {
    current: 1,
    total: 0,
    pageSize: 0,
  }
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_DATA_PROJECT:
      return {
        ...state,
        loading: true,
        data: null,
        pagination: {
          current: 1,
          total: 0,
          pageSize: 0,
        }
      }
    case LOAD_DATA_PROJECT_SUCCESS:
      return {
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS',
        pagination: {
          current: action.payload.pagination.current,
          total: action.payload.pagination.total,
          pageSize: action.payload.pagination.pageSize
        }
      }
    case LOAD_DATA_PROJECT_FAILED:
      return {
        ...state, 
        loading: false,
        data: null,
        message: 'FAILED',
        pagination: {
          total: 0,
          current: 1,
          pageSize: 0,
        }
      }
    case UNMOUNT_DATA_PROJECT:
      return { 
        ...state, 
        loading: true,
        data: null,
        pagination: {
          total: 0,
          current: 1,
          pageSize: 0,
        }
      }
    default:
      return state
  }
}