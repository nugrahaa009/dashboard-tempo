const LOAD_PROJECT_DETAIL         = 'LOAD_PROJECT_DETAIL'
const LOAD_PROJECT_DETAIL_SUCCESS = 'LOAD_PROJECT_DETAIL_SUCCESS'
const LOAD_PROJECT_DETAIL_FAILED  = 'LOAD_PROJECT_DETAIL_FAILED'
const UNMOUNT_PROJECT_DETAIL      = 'UNMOUNT_PROJECT_DETAIL'

const initialState = {
  data: null,
  loading: true,
  message: null
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_PROJECT_DETAIL:
      return {
        ...state,
        loading: true
      }
    case LOAD_PROJECT_DETAIL_SUCCESS:
      return {
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS'
      }
    case LOAD_PROJECT_DETAIL_FAILED:
      return {
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED'
      }
    case UNMOUNT_PROJECT_DETAIL:
      return { 
        ...state, 
          loading: true,
          data: null,
      }
    default:
      return state
  }
}