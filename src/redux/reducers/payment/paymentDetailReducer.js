const LOAD_PAYMENT_DETAIL         = 'LOAD_PAYMENT_DETAIL'
const LOAD_PAYMENT_DETAIL_SUCCESS = 'LOAD_PAYMENT_DETAIL_SUCCESS'
const LOAD_PAYMENT_DETAIL_FAILED  = 'LOAD_PAYMENT_DETAIL_FAILED'
const UNMOUNT_DETAIL_PAYMENT      = 'UNMOUNT_DETAIL_PAYMENT'

const initialState = {
  data: null,
  loading: true,
  message: null
}

export default (state = initialState, action = {}) => {
  switch(action.type){
    case LOAD_PAYMENT_DETAIL:
      return{
        ...state,
        loading: true
      }
    case LOAD_PAYMENT_DETAIL_SUCCESS:
      return{ 
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS'
      }
    case LOAD_PAYMENT_DETAIL_FAILED:
      return{ 
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED'
      }
      case UNMOUNT_DETAIL_PAYMENT:
        return{ 
          ...state, 
          loading: true,
          data: null,
        }
    default:
      return state
  }
}