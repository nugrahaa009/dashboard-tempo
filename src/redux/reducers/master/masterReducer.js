const LIST_DATA_MASTER          = 'LIST_DATA_MASTER';
const LIST_DATA_MASTER_SUCCESS  = 'LIST_DATA_MASTER_SUCCESS';
const LIST_DATA_MASTER_FAILED   = 'LIST_DATA_MASTER_FAILED';
const UNMOUNT_DATA_MASTER       = 'UNMOUNT_DATA_MASTER';

const initialState = {
  loading: true,
  data: null,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LIST_DATA_MASTER:
      return {
        ...state,
        loading: true,
      };
    case LIST_DATA_MASTER_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    case LIST_DATA_MASTER_FAILED:
      return {
        ...state,
        loading: false,
        data: null,
      };
    case UNMOUNT_DATA_MASTER:
      return {
        ...state,
        loading: true,
        data: null,
      };
    default:
      return state;
  }
}
