const LOAD_DASHBOARD          = 'LOAD_DASHBOARD'
const LOAD_DASHBOARD_SUCCESS  = 'LOAD_DASHBOARD_SUCCESS'
const LOAD_DASHBOARD_FAILED   = 'LOAD_DASHBOARD_FAILED'
const UNMOUNT_DASHBOARD       = 'UNMOUNT_DASHBOARD'

const initialState = {
  loading: true,
  data: null,
  message: null,
}

export default (state = initialState, action = {}) => {
  switch(action.type){
    case LOAD_DASHBOARD:
      return{
        ...state,
        loading: true,
        data: null,
      }
    case LOAD_DASHBOARD_SUCCESS:
      return{ 
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS',
      }
    case LOAD_DASHBOARD_FAILED:
      return{ 
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED',
      }
    case UNMOUNT_DASHBOARD:
      return{ 
          ...state, 
        loading: true,
        data: null,
      }
    default:
      return state
  }
}
