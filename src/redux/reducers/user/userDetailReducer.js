const LOAD_USER_DETAIL         = 'LOAD_USER_DETAIL'
const LOAD_USER_DETAIL_SUCCESS = 'LOAD_USER_DETAIL_SUCCESS'
const LOAD_USER_DETAIL_FAILED  = 'LOAD_USER_DETAIL_FAILED'
const UNMOUNT_DETAIL_USER      = 'UNMOUNT_DETAIL_USER'

const initialState = {
  data: null,
  loading: true,
  message: null,
  pagination: {
    current: 1,
    total: 0,
    pageSize: 0,
  }
}

export default (state = initialState, action = {}) => {
  switch(action.type){
    case LOAD_USER_DETAIL:
      return{
        ...state,
        loading: true,
        pagination: {
          current: 1,
          total: 0,
          pageSize: 0,
        }
      }
    case LOAD_USER_DETAIL_SUCCESS:
      return{ 
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS',
        pagination: {
          current: action.payload.pagination.current,
          total: action.payload.pagination.total,
          pageSize: action.payload.pagination.pageSize
        }
      }
    case LOAD_USER_DETAIL_FAILED:
      return{ 
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED',
        pagination: {
          current: 1,
          total: 0,
          pageSize: 0,
        }
      }
      case UNMOUNT_DETAIL_USER:
        return{ 
          ...state, 
          loading: true,
          data: null,
          pagination: {
            current: 1,
            total: 0,
            pageSize: 0,
          }
        }
    default:
      return state
  }
}