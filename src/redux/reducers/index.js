import { combineReducers } from 'redux'
import auth from './auth/authReducer'
import master from './master/masterReducer'
import dashboard from './dashboard/dashboardReducer'
import listUser from './user/userReducer'
import detailUser from './user/userDetailReducer'
import listPayment from './payment/paymentReducer'
import detailPayment from './payment/paymentDetailReducer'
import listInvoice from './invoice/invoiceReducer'
import detailInvoice from './invoice/invoiceDetailReducer'
import listPlanning from './planning/planningReducer'
import detailPlanning from './planning/planningDetailReducer'
import listProject from './project/projectReducer'
import detailProject from './project/projectDetailReducer'
import listSkill from './skill/skillReducer'
import detailSkill from './skill/skillDetailReducer'

export default combineReducers({
  auth,
  master,
  dashboard,
  user: combineReducers({ 
    list      : listUser,
    detail    : detailUser,
  }),
  payment: combineReducers({ 
    list      : listPayment,
    detail    : detailPayment,
  }),
  invoice: combineReducers({ 
    list      : listInvoice,
    detail    : detailInvoice,
  }),
  planning: combineReducers({ 
    list      : listPlanning,
    detail    : detailPlanning,
  }),
  project: combineReducers({ 
    list      : listProject,
    detail    : detailProject,
  }),
  skill: combineReducers({ 
    list      : listSkill,
    detail    : detailSkill,
  }),
})