const LOAD_DATA_PLANNING         = 'LOAD_DATA_PLANNING'
const LOAD_DATA_PLANNING_SUCCESS = 'LOAD_DATA_PLANNING_SUCCESS'
const LOAD_DATA_PLANNING_FAILED  = 'LOAD_DATA_PLANNING_FAILED'
const UNMOUNT_DATA_PLANNING      = 'UNMOUNT_DATA_PLANNING'

const initialState = {
  loading: true,
  data: null,
  message: null,
  pagination: {
    current: 1,
    total: 0,
    pageSize: 0,
  }
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_DATA_PLANNING:
      return {
        ...state,
        loading: true,
        data: null,
        pagination: {
          current: 1,
          total: 0,
          pageSize: 0,
        }
      }
    case LOAD_DATA_PLANNING_SUCCESS:
      return {
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS',
        pagination: {
          current: action.payload.pagination.current,
          total: action.payload.pagination.total,
          pageSize: action.payload.pagination.pageSize
        }
      }
    case LOAD_DATA_PLANNING_FAILED:
      return {
        ...state, 
        loading: false,
        data: null,
        message: 'FAILED',
        pagination: {
          current: 1,
          total: 0,
          pageSize: 0,
        }
      }
    case UNMOUNT_DATA_PLANNING:
      return { 
        ...state, 
        loading: true,
        data: null,
        pagination: {
          current: 1,
          total: 0,
          pageSize: 0,
        }
      }
    default:
      return state
  }
}