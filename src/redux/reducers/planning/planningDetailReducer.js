const LOAD_PLANNING_DETAIL         = 'LOAD_PLANNING_DETAIL'
const LOAD_PLANNING_DETAIL_SUCCESS = 'LOAD_PLANNING_DETAIL_SUCCESS'
const LOAD_PLANNING_DETAIL_FAILED  = 'LOAD_PLANNING_DETAIL_FAILED'
const UNMOUNT_DETAIL_PLANNING      = 'UNMOUNT_DETAIL_PLANNING'

const initialState = {
  data: null,
  loading: true,
  message: null
}

export default (state = initialState, action = {}) => {
  switch(action.type){
    case LOAD_PLANNING_DETAIL:
      return{
        ...state,
        loading: true
      }
    case LOAD_PLANNING_DETAIL_SUCCESS:
      return{ 
        ...state, 
        loading: false,
        data: action.payload.data,
        message: 'SUCCESS'
      }
    case LOAD_PLANNING_DETAIL_FAILED:
      return{ 
        ...state, 
        loading: true,
        data: null,
        message: 'FAILED'
      }
      case UNMOUNT_DETAIL_PLANNING:
        return{ 
          ...state, 
          loading: true,
          data: null,
        }
    default:
      return state
  }
}