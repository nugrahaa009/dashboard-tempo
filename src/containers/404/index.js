import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Result, Button } from 'antd'
import { Link } from 'react-router-dom'

export class NotFound extends Component {
  render() {
    return (
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Link to="/dashboard">
            <Button type="primary">Back Home</Button>
          </Link>
        }
      />
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(NotFound)
