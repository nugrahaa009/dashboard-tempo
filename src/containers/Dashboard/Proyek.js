import React, { Component } from 'react';
import { number } from '../../helper/pagination';
import { Card, Col, Row, Typography, Progress, Table } from 'antd';
const { Title, Text } = Typography;

export default class Proyek extends Component {
  render() {
    const { current, perPage, data, pagination } = this.props;

    const columns = [
      {
        title: 'No',
        ellipsis: true,
        align: 'left',
        width: 80,
        render: (data, text, index) => number(index, current, perPage)
      },
      {
        title: 'Nama Proyek',
        width: 200,
        render: (record) => (
          <Text>
            {record ? record.name : '-'} 
          </Text>
        )
      },
      {
        title: 'Progress',
        render: (record) => (
          <Progress
            strokeLinecap="square"
            strokeColor={{
              '0%': '#108ee9',
              '100%': '#87d068',
            }}
            percent={record.progress}
          />
        )
      },
    ]
    return (
      <React.Fragment>
        <Card>
          <Row gutter={[8, 16]}>
            <Col span={24}>
              <Title level={4}>
                Laporan Proyek
              </Title>
            </Col>
            <Col span={24}>
              <Table 
                rowKey={(i) => i.uid}
                dataSource={data} 
                columns={columns} 
                pagination={pagination} 
                onChange={this.props.handlePagination} 
              />
            </Col>
          </Row>
        </Card>
      </React.Fragment>
    )
  }
}
