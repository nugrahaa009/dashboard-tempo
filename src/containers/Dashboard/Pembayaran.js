import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';
import { Card, Col, Row, Typography } from 'antd';
const { Title } = Typography;

export default class Pembayaran extends Component {
  render() {
    const { getDashboard } = this.props;

    const dataChart = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
      datasets: [{
				label: 'Pengeluaran',
				backgroundColor: '#2711A2',
				data: getDashboard && getDashboard.projectPayment
			}]
    }

    return (
      <React.Fragment>
        <Card>
          <Row gutter={[8, 16]}>
            <Col span={24}>
              <Title level={4}>
                Laporan Pembayaran
              </Title>
            </Col>
            <Col span={24}>
              <Bar data={dataChart} />
            </Col>
          </Row>
        </Card>
      </React.Fragment>
    )
  }
}
