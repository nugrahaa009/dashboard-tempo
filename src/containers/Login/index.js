import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Form, Input, Card, Button, Row, Col, message } from 'antd'
import { setLogin } from '../../redux/actions/auth/authAction'

export class Login extends Component {

  onFinish =  async (values) => {
    let { actionSetLogin } = this.props;
    return actionSetLogin(values, null, (err) => {
      return message.error(err.message);
    })
  }

  render() {
    return (
      <React.Fragment>
        <Row align="middle" justify="center" style={{ height: '100vh' }}>
          <Col>
            <Card style={{ maxWidth: 500, padding: '50px 50px', boxShadow: '0px 4px 24px rgba(0, 0, 0, 0.1)' }}>
              <Form name="normal_login" className="login-form" initialValues={{ remember: true }} onFinish={this.onFinish}>
                <Row gutter={16}>

                  <Col sm={24} md={24} style={{ textAlign: 'center', marginBottom: 30 }}>
                    <div className="logo">
                      <img src={require('../../assets/img/logo-login.png')} alt="" style={{ width: '40%' }} />
                    </div>
                  </Col>

                  <Col sm={24} md={24}>
                    <Form.Item name="userName" rules={[{ required: true, message: 'Username Tidak Boleh Kosong!!' }]}>
                      <Input placeholder="Username" size="large" />
                    </Form.Item>
                  </Col>

                  <Col sm={24} md={24}>
                    <Form.Item name="password" rules={[{ required: true, message: 'Password Tidak Boleh Kosong!!' }]}>
                      <Input type="password" placeholder="Password" size="large"/>
                    </Form.Item>
                  </Col>

                  <Col sm={24} md={24}>
                    <Form.Item>
                      <Button type="primary" htmlType="submit" className="login-form-button" size="large" block>
                        Masuk
                      </Button>
                    </Form.Item>
                  </Col>

                </Row>
              </Form>
            </Card>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  actionSetLogin: setLogin
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
