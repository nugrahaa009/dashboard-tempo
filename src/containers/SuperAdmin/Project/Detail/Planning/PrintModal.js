import React, { Component } from 'react'
import { Modal, Row, Col, Button, Typography, Spin } from 'antd'
const { Text } = Typography;

export default class PrintModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
       part: null,
       from: null,
       to: null
    }
  }
  render() {
    const { isVisible, onOk, loading, data: { totalProject } } = this.props;
    const { part, from, to } = this.state;
    const num = 5000;
    const result = Math.ceil(totalProject / num);
    return (
      <Modal 
        width={900} 
        title="PRINT PERENCANAAN PROYEK" 
        visible={isVisible} 
        onOk={() => onOk()} 
        onCancel={() => this.onCancel()}
        footer={null}
        // closable={false}
        maskClosable={false}
      >
        <Spin tip="Loading..." spinning={loading}>
          <Row  gutter={[16, 16]}>
            <Col span={24}>
              <Row>
                <Col span={12}>Total Data: {totalProject}</Col>
                <Col span={12} style={{textAlign: 'right'}}>
                  {totalProject >= num &&  `Note: ${num}/File` }
                </Col>
              </Row>
            </Col>
            {new Array(result).fill().map((_, i) => (
              <Col span={6} key={i}>
                <Button 
                  type={i+1 === this.state.part ? "primary" : "ghost"} 
                  size="large" 
                  block 
                  onClick={() => this.onClickBtn(i, this.note(i, num, result, totalProject))}
                >
                  Part {i+1}
                </Button>
                <Text type="secondary" style={{fontStyle: 'italic', fontSize: 12}}>
                  *{this.note(i, num, result, totalProject).text}
                </Text>
              </Col>
            ))}
            <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end', marginTop: 50}}>
              <Button disabled={this.state.part === null} block onClick={() => onOk(this.state)} size="large" type="primary" className="register-form-button" style={{ marginRight: 10 }}>
                Download Part {part ? `${part} (${from.toLocaleString()} - ${to.toLocaleString()})` : "..."}
              </Button>
            </Col>
          </Row>
        </Spin>
      </Modal>
    )
  }
  
  onClickBtn(i, note){
    var index = i + 1;
    return this.setState({ 
      part: index ,
      from: note.from,
      to: note.to
    })
  }

  onCancel(){
    const { onCancel } = this.props;
    return this.setState({ part: null }, () => onCancel())
  }

  note(i, num, result, totalProject){
    var index = i + 1;
    var from = index === 1 ? 1 : (num*i)+1;
    var to = index === result ? totalProject : num*(index);
    return{
      text: `${from.toLocaleString()} - ${to.toLocaleString()}`,
      from: from,
      to: to
    };
  }
}
