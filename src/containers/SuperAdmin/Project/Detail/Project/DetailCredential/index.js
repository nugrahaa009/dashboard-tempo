import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../../../components'
import { Col, Form, Input, Row, Button, message, Divider } from 'antd'
import { detailProject, editProject, unmountDetailProject } from '../../../../../../redux/actions/project/projectAction'

export class DetailProjectCredential extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       submitLoading: false,
    }
  }

  componentDidMount(){
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid, 'description')
  }

  onFinish = (values) => {
    const { uid, actionSetEdit, onCloseModal, unmountGetDetail } = this.props
    const payload = {
      uid: uid,
      description: values,
      type: "description"
    }
    this.setState({ submitLoading: true })
    actionSetEdit(payload, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        message.success(response.status.message)
        onCloseModal()
        return unmountGetDetail()
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }

  onCancel = () => {
    const { onCancel } = this.props
    return onCancel()
  }
  
  render() {
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
          <Row gutter={16}>
            {
              Object.entries(data).map((item, i) => (
                <React.Fragment key={i}>
                  <Col sm={24} md={12} key={i}>
                    <Form.Item key={i} name={item[0]} label={item[0]} initialValue={item[1]}>
                      <Input size="large" />
                    </Form.Item>
                  </Col>
                </React.Fragment>
              ))
            }
          </Row>

          <Divider />

          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button size="large" type="danger" onClick={() => this.onCancel()} style={{ marginRight: 10 }}>
              Batal
            </Button>
            <Button size="large" type="primary" htmlType="submit" loading={this.state.submitLoading}>
              Simpan
            </Button>
          </div>

        </Form>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionSetEdit     : editProject,
  actionGetDetail   : detailProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProjectCredential)
