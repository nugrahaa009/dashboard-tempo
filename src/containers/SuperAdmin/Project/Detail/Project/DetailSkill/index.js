import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../../../components'
import { detailProject, unmountDetailProject, editProject } from '../../../../../../redux/actions/project/projectAction'
import { Form, Row, Col, Button, message, Select, Divider } from 'antd'

export class DetailProjectSkill extends Component {
  constructor(props) {
    super(props)
    this.state = {
       submitLoading: false,
       _loadData: false
    }
  }

  componentDidMount(){
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid, 'skill');
  }

  onFinish = (values) => {
    const { onCloseModal, uid, actionSetEdit, unmountGetDetail } = this.props
    const payload = {
      skill: values,
      uid: uid,
      type: 'skill'
    }
    this.setState({ submitLoading: true })
    actionSetEdit(payload, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        message.success(response.status.message)
        onCloseModal()
        return unmountGetDetail()
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }

  onCancel = () => {
    const { onCancel } = this.props
    return onCancel()
  }

  render() {
    const { getData: { loading, data } } = this.props

    if(loading){
      return <Loading />
    }
    
    return (
      <React.Fragment>
        <Form 
          onFinish={this.onFinish} 
          name="normal_register" 
          layout="vertical"
        >
          <Row style={{marginBottom: 20}}>
            {
              data.length > 0 && data.map((item, i) => (
                <React.Fragment key={i}>
                  {
                    Object.entries(item).length > 0 && Object.entries(item).map((item_, i) => (
                      <Col sm={24} md={12} key={i}>
                        <Form.Item key={i} name={item_[0]} label={item_[0]} initialValue={item_[1].value && item_[1].value.value}>
                          <Select
                            showSearch
                            size="large"
                            style={{ width: '98%' }}
                            placeholder="Pilih Tukang"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {
                              item_[1].user.length > 0 && item_[1].user.map((res, i) => (
                                <Select.Option key={i} value={ res.value}>
                                  {`${res.name} (${res.staff})`}
                                </Select.Option>
                              ))
                            }
                          </Select>
                        </Form.Item>
                      </Col>
                    ))
                  }
                </React.Fragment>
              ))
            }
          </Row> 

          <Divider />
          
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button size="large" type="danger" onClick={() => this.onCancel()} style={{ marginRight: 10 }}>
              Batal
            </Button>
            <Button size="large" type="primary" htmlType="submit" loading={this.state.submitLoading}>
              Simpan
            </Button>
          </div>
        </Form>
      </React.Fragment>
    )
  }

  componentWillUnmount(){
    const { unmountGetDetail } = this.props;
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionSetEdit     : editProject,
  actionGetDetail   : detailProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProjectSkill)
