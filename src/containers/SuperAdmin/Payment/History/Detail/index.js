import React, { Component } from 'react'
import NumberFormat from 'react-number-format'
import { Loading } from '../../../../../components'
import { columns } from './columns'
import { connect } from 'react-redux'
import { PageHeader, Row, Col, Table, Typography, Button, Modal } from 'antd'
import { ExclamationCircleOutlined, FileExcelOutlined, FilePdfOutlined } from '@ant-design/icons'
import { detailHistory, unmountDetailHistory } from '../../../../../redux/actions/payment/paymentAction'
const { Title } = Typography
const { confirm } = Modal

export class DetailHistory extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       current: 1
    }
  }

  componentDidMount() {
    const { actionGetDetail, match: { params } } = this.props
    return actionGetDetail(params.id)
  }

  exportPDF = () => {
    const { getData: { data } } = this.props
    confirm({
      title: 'Apa anda ingin melakukan print pembayaran?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Ya',
      okType: 'primary',
      cancelText: 'Batal',
      onOk() {
        window.open(data[0].invoicePath)
      },
    });
  }

  exportExcel = () => {
    const { getData: { data } } = this.props
    confirm({
      title: 'Apa anda ingin melakukan print pembayaran?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Ya',
      okType: 'primary',
      cancelText: 'Batal',
      onOk() {
        window.open(data[0].pathExcel)
      },
    });
  }

  render() {
    const { current } = this.state
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="DETAIL RIWAYAT" onBack={() => this.props.history.push('/history')} />
        <Row gutter={16}>
          <Col flex={1} style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: 30 }}>
            <Button type="primary" size="large" icon={<FileExcelOutlined />} onClick={() => this.exportExcel()} style={{ marginRight: 15 }}>PRINT EXCEL</Button>
            <Button type="primary" size="large" icon={<FilePdfOutlined />} onClick={() => this.exportPDF()}>PRINT PDF</Button>
          </Col>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={24} md={24}>
                <Table 
                  rowKey={this.state.current}
                  dataSource={data} 
                  columns={columns(current)} 
                  loading={loading} 
                  pagination={false}
                  summary={pageData => {
                    let _totalPrice = 0;
                    pageData.forEach(({ totalPrice }) => {
                      _totalPrice += totalPrice;
                    });
            
                    return (
                      <>
                        <Table.Summary.Row style={{ textAlign: 'center' }}>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <Title level={3}>Jumlah</Title>
                          </Table.Summary.Cell>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <NumberFormat 
                              value={_totalPrice ? _totalPrice : 0} 
                              displayType={'text'} 
                              thousandSeparator={true} 
                              prefix={'Rp '} 
                              renderText={value => <Title level={3}>{value}</Title>} 
                            />
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                      </>
                    );
                  }}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.invoice.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailHistory,
  unmountGetDetail  : unmountDetailHistory
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailHistory)
