import React, { Component } from 'react'
import { columns } from './columns'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { FilterHistory } from '../../../../components'
import { PageHeader, Menu, Typography, Table, Row, Col, Modal } from 'antd'
import { ExclamationCircleOutlined, DollarOutlined, HistoryOutlined } from '@ant-design/icons'
import { listHistory, unmountDataHistory } from '../../../../redux/actions/payment/paymentAction'
const qs = require('qs')
const { Text } = Typography
const { confirm } = Modal

export class History extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(current.page, this.state.perPage, filter)
      })
    }else{
      return actionGetData(1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(e.current, e.pageSize, this.state.filter);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { history, actionGetData } = this.props
    const _filter = {
      type: 'name',
      search: e.name !== undefined ? e.name : null,
      orderBy: 'desc',
      dateFrom: e.dateFrom,
      dateTo: e.dateTo
    }
    return this.setState({ "current": 1, "filter": _filter  }, () => {
      history.push()
      window.scrollTo(0, 0);
      return actionGetData(this.state.curent, this.state.perPage, this.state.filter)
    })
  }

  exportExcel = (path) => {
    confirm({
      title: 'Apa anda ingin melakukan print pembayaran?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Ya',
      okType: 'primary',
      cancelText: 'Batal',
      onOk() {
        window.open(path)
      },
    });
  }

  exportPDF = (path) => {
    confirm({
      title: 'Apa anda ingin melakukan print pembayaran?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Ya',
      okType: 'primary',
      cancelText: 'Batal',
      onOk() {
        window.open(path)
      },
    });
  }

  render() {
    const { current, perPage } = this.state
    const { getData: { data, loading } } = this.props
    return (
      <React.Fragment>
        <PageHeader 
          className="site-page-header" 
          title="RIWAYAT" 
          ghost={true}
          extra={[
            <Menu key="1" selectedKeys="1" mode="horizontal" style={{ background: 'none' }}>
              <Menu.Item key="1" icon={<HistoryOutlined />}>
                <Link to={`/history`}>
                  <Text>RIWAYAT</Text>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<DollarOutlined />}>
                <Link to={`/payment`}>
                  <Text>PEMBAYARAN</Text>
                </Link>
              </Menu.Item>
            </Menu>,
          ]}
        />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterHistory onSearch={this.onSearch} />
          </Col>
          <Col sm={24} md={24}>
            <Table 
              rowKey={(i) => i.invoiceNumber}
              dataSource={data} 
              columns={columns(current, perPage, this.exportExcel, this.exportPDF)} 
              loading={loading}  
            />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  getData: state.invoice.list
})

const mapDispatchToProps = {
  actionGetData   : listHistory,
  unmountGetData  : unmountDataHistory
}

export default connect(mapStateToProps, mapDispatchToProps)(History)
