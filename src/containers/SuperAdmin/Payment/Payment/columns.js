import React from 'react'
import { Link } from 'react-router-dom'
import { number } from '../../../../helper/pagination'
import { Button, Space, Tag, Typography, Tooltip, Dropdown, Menu } from 'antd'
import { PrinterOutlined, ExclamationCircleOutlined, SearchOutlined, FilePdfOutlined, FileExcelOutlined } from '@ant-design/icons'
import NumberFormat from 'react-number-format'
const { Text } = Typography

const menu = (exportPDF, exportExcel, uid) => (
  <Menu>
    <Menu.Item icon={<FilePdfOutlined />} onClick={() => exportPDF(uid)}>Export PDF</Menu.Item>
    <Menu.Item icon={<FileExcelOutlined />} onClick={() => exportExcel(uid)}>Export Excel</Menu.Item>
  </Menu>
)

export const columns = (current, perPage, exportPDF, exportExcel) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 80,
      render: (data, text, index) => number(index, current, perPage)
    },
    {
      title: 'Nama Proyek',
      render: (record) => (
        <Text>
          {record.planProject.name ? record.planProject.name : '-'} 
        </Text>
      )
    },
    {
      title: 'Jumlah Pembayaran',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.totalPrice ? record.totalPrice.toLocaleString() : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          prefix={'Rp '} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
    {
      title: 'Status Pembayaran',
      align: 'center',
      render: () => (
        <React.Fragment>
          <Tag icon={<ExclamationCircleOutlined />} color="error">
            Belum Dibayar
          </Tag>
        </React.Fragment>
      )
    },
    {
      title: '',
      align: 'right',
      render: (record) => (
        <Space>
          <Link to={`/payment/detail/${record.planProjectId}`}>
            <Tooltip placement="bottomRight" title="Lihat Detail Pembayaran">
              <Button type="primary" shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
          </Link>
          <Dropdown overlay={menu(exportPDF, exportExcel, record.planProject.uid)}>
            <Button type="danger" shape="circle" icon={<PrinterOutlined />} />
          </Dropdown>
        </Space>
      )
    },
  ]
}