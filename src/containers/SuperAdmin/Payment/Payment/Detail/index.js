import React, { Component } from 'react'
import NumberFormat from 'react-number-format'
import { Loading } from '../../../../../components'
import { columns } from './columns'
import { connect } from 'react-redux'
import { PageHeader, Row, Col, Typography, Table, Button, Modal, message } from 'antd'
import { ExclamationCircleOutlined, FilePdfOutlined, FileExcelOutlined } from '@ant-design/icons'
import { detailPayment, exportExcel, unmountDetailPayment } from '../../../../../redux/actions/payment/paymentAction'
const { Text, Title } = Typography
const { confirm } = Modal

export class DetailPayment extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       current: 1
    }
  }

  componentDidMount() {
    const { actionGetDetail, match: { params } } = this.props
    return actionGetDetail(params.id)
  }

  exportPDF = () => {
    const { actionOnExport, history, match: { params } } = this.props
    const payload = {
      'uid': params.id
    }
    confirm({
      title: 'Apa anda ingin melakukan print pembayaran?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Ya',
      okType: 'primary',
      cancelText: 'Batal',
      onOk() {
        return actionOnExport(payload, (response) => {
          const download = response.body.pdf
          window.open(download)
          message.success(response.status.message)
          history.push('/payment')

        }, (err) => message.error(err.message))
      },
    });
  }

  exportExcel = () => {
    const { actionOnExport, history, match: { params } } = this.props
    const payload = {
      'uid': params.id
    }
    confirm({
      title: 'Apa anda ingin melakukan print pembayaran?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Ya',
      okType: 'primary',
      cancelText: 'Batal',
      onOk() {
        return actionOnExport(payload, (response) => {
          const download = response.body.excel
          window.open(download)
          message.success(response.status.message)
          history.push('/payment')
        }, (err) => message.error(err.message))
      },
    });
  }
  
  render() {
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="DETAIL PEMBAYARAN" onBack={() => this.props.history.push('/payment')} />
        <Row justify="space-around" align="middle" style={{marginBottom: 30}}>
          <Col flex={4}>
            <Text strong>Status: </Text>
            <Text type="danger">Belum Bayar</Text>
          </Col>
          <Col flex={1} style={{ display: 'flex', justifyContent: 'flex-end'}}>
            <Button type="primary" size="large" icon={<FileExcelOutlined />} onClick={() => this.exportExcel()} style={{ marginRight: 15 }}>PRINT EXCEL</Button>
            <Button type="primary" size="large" icon={<FilePdfOutlined />} onClick={() => this.exportPDF()}>PRINT PDF</Button>
          </Col>
        </Row>
        <Row gutter={16}>

          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={24} md={24}>
                <Table 
                  rowKey={this.state.current}
                  dataSource={data} 
                  columns={columns(this.state.current)} 
                  loading={loading} 
                  pagination={false}
                  summary={pageData => {
                    let _totalPrice = 0;

                    pageData.forEach(({ totalPrice }) => {
                      _totalPrice += totalPrice;
                    });
            
                    return (
                      <>
                        <Table.Summary.Row style={{ textAlign: 'center' }}>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <Title level={3}>Jumlah</Title>
                          </Table.Summary.Cell>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <NumberFormat 
                              value={_totalPrice ? _totalPrice : 0} 
                              displayType={'text'} 
                              thousandSeparator={true} 
                              prefix={'Rp '} 
                              renderText={value => <Title level={3}>{value}</Title>} 
                            />
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                      </>
                    );
                  }}
                />
              </Col>
            </Row>

          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.payment.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailPayment,
  actionOnExport    : exportExcel,
  unmountGetDetail  : unmountDetailPayment
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPayment)
