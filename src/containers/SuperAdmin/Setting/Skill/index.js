import React, { Component } from 'react'
import CreateSkill from './Create'
import DetailSkill from './Detail'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { columns } from './columns'
import { FilterTable } from '../../../../components'
import { PageHeader, Row, Col, Button, Table, Modal, Menu, Typography } from 'antd'
import { PlusOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons'
import { listSkill, detailSkill, unmountDataSkill } from '../../../../redux/actions/skill/skillAction'

const qs = require('qs')

const { Text } = Typography

export class Skill extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc'
      },
      _visible: false,
      _type: null,
      _uid: null
    }
  }

  componentDidMount() {
    const { actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    return this.setState({ current: current.page || 1, filter: { ...this.state.filter, search: current.search || null } }, () => {
      return actionGetData(current.page, this.state.perPage, this.state.filter)
    })
  }

  pagination = (e) => {
    const { history, actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    return this.setState({ current: e.current, perPage: e.pageSize, filter: { ...this.state.filter, search: current.search || null } }, () => {
      actionGetData(e.current, e.pageSize, this.state.filter);
      const params = current.search ? `page=${e.current}&search=${current.search}` : `page=${e.current}`
      return history.push({
        search: params
      })
    })
  }

  onSearch = (e) => {
    const { history, actionGetData } = this.props
    return this.setState({ "current": 1, "filter": e }, () => {
      history.push({
        search: `search=${e.search}`
      })
      window.scrollTo(0, 0);
      return actionGetData(this.state.current, this.state.perPage, this.state.filter);
    })
  }

  renderModal(_type, _uid){
    switch (_type) {
      case 'create':
        return <CreateSkill onCloseModal={this.onCloseModal} onRefresh={this.onRefresh} current={this.state.current} />
      case 'edit':
        return <DetailSkill onCloseModal={this.onCloseModal} onRefresh={this.onRefresh} uid={_uid} current={this.state.current} />
      default:
        return null
    }
  }

  renderTitle(_type){
    switch (_type) {
      case 'create':
        return <Text>Buat Skill</Text>
      case 'edit':
        return <Text>Ubah Kemampuan</Text>
      default:
        return null
    }
  }

  onRefresh = () => {
    const { actionGetData } = this.props
    return actionGetData(this.state.current, this.state.perPage, this.state.filter)
  }

  onCloseModal = () => {
    return this.setState({
      _visible: false,
      _type: null,
      _uid: null
    })
  }

  openModal(type, uid){
    const { actioGetDetail } = this.props
    type === 'edit' && actioGetDetail(uid);
    return this.setState({
      _visible: true,
      _type: type,
      _uid: uid
    })
  }

  render() {
    const { current, perPage, _visible, _type, _uid } = this.state
    const { getData: { data, loading, pagination } } = this.props
    const defaultStatusFilter = [
      { value: 'name', name: 'Nama Kemampuan' },
    ]
    return (
      <React.Fragment>
        <PageHeader 
          className="site-page-header" 
          title="KEMAMPUAN" 
          ghost={true}
          extra={[
            <Menu key="1" selectedKeys="1" mode="horizontal" style={{ background: 'none' }}>
              <Menu.Item key="1" icon={<SettingOutlined />}>
                <Link to="/setting">
                  <Text>KEMAMPUAN</Text>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<UserOutlined />}>
                <Link to="/profile/edit">
                  <Text>UBAH PROFIL</Text>
                </Link>
              </Menu.Item>
            </Menu>,
          ]}
        />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterTable 
              fase1={null}
              fase2={{ key: 'type', value: defaultStatusFilter}}
              onSearch={this.onSearch}
            />
          </Col>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Button type="dashed" size="large"  icon={<PlusOutlined />} onClick={() => this.openModal('create')} block>
              Tambah Kemampuan
            </Button> 
          </Col>
          <Col sm={24} md={24}>
            <Table 
              rowKey={(i) => i.id}
              dataSource={data} 
              columns={columns(current, perPage, (type, uid) => this.openModal(type, uid))} 
              loading={loading} 
              pagination={pagination} 
              onChange={this.pagination}
            />
          </Col>
        </Row>

        <Modal
          title={this.renderTitle(_type)}
          visible={_visible}
          onOk={this.onCloseModal}
          // onCancel={this.onCloseModal}
          closable={false}
          centered
          footer={false}
          width={1000}
        >
          {this.renderModal(_type, _uid)}
        </Modal>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData: state.skill.list
})

const mapDispatchToProps = {
  actionGetData   : listSkill,
  actioGetDetail    : detailSkill,
  unmountGetData  : unmountDataSkill
}

export default connect(mapStateToProps, mapDispatchToProps)(Skill)
