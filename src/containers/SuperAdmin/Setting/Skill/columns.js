import React from 'react'
import { number } from '../../../../helper/pagination'
import { Button, Space, Typography } from 'antd'
import { EditOutlined } from '@ant-design/icons'
import NumberFormat from 'react-number-format';
const { Text } = Typography

export const columns = (current, perPage, openModal) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 80,
      render: (data, text, index) => number(index, current, perPage)
    },
    {
      title: 'Nama Kemampuan',
      render: (record) => (
        <Text>
          {record ? record.skillName : '-'} 
        </Text>
      )
    },
    {
      title: 'Harga Satuan',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.price ? record.price.toLocaleString() : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          prefix={'Rp '} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
    {
      title: '',
      align: 'right',
      render: (record) => (
        <Space>
          <Button type="primary" shape="circle" icon={<EditOutlined />} onClick={() => openModal('edit', record.id)} />
          {/* <Button type="danger" shape="circle" icon={<DeleteOutlined />} onClick={() => deleteConfirm(record.id)} /> */}
        </Space>
      )
    },
  ]
}