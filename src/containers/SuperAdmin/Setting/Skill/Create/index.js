import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Form, Input, Row, Col, Button, message, Space } from 'antd'
import { createSkill } from '../../../../../redux/actions/skill/skillAction';

export class CreateSkill extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      submitLoading: false
    }
  }

  onFinish = (values) => {
    const { actionSetCreate, onCloseModal, onRefresh } = this.props
    this.setState({ submitLoading: true })
    return actionSetCreate(values, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        onCloseModal();
        onRefresh()
        return message.success(response.status.message);
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }

  onCancel = () => {
    const { onCloseModal } = this.props
    return onCloseModal()
  }

  render() {
    return (
      <React.Fragment>
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
          <Row gutter={16}>
            <Col sm={24} md={12}>
              <Form.Item label="Nama Skill" name="name" rules={[{ required: true, message: 'Nama Kemampuan Tidak Boleh Kosong!' }]}>
                <Input placeholder="Nama Skill" size="large" />
              </Form.Item>
            </Col>
            <Col sm={24} md={12}>
              <Form.Item 
                label="Harga Satuan" 
                name="price"
                rules={[
                  { required: true, message: 'Harga Satuan Tidak Boleh Kosong!' },
                  { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                ]}
              >
                <Input size="large" placeholder="Harga Satuan" addonBefore="Rp" style={{ width: '100%' }} />
              </Form.Item>
            </Col>

            <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Form.Item>
                <Space size={16}>
                  <Button size="large" type="danger" onClick={() => this.onCancel()}>
                    Kembali
                  </Button>
                  <Button size="large" type="primary" htmlType="submit" className="register-form-button" loading={this.state.submitLoading}>
                    Tambah Skill
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  actionSetCreate: createSkill,
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSkill)
