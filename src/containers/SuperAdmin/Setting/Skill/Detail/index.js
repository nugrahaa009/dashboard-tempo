import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../../components'
import { Form, Input, Row, Col, Button, message, Divider } from 'antd'
import { detailSkill, editSkill, unmountDetailSkill } from '../../../../../redux/actions/skill/skillAction'

export class DetailSkill extends Component {
  constructor(props) {
    super(props)
    this.state = {
      submitLoading: false
    }
  }
  
  onFinish = (values) => {
    const { actionSetEdit, uid, onCloseModal, onRefresh } = this.props
    const newValue = values
    newValue.id = uid
    return this.setState({ submitLoading: true }, () => {
      return actionSetEdit(values, async (response) => {
        return this.setState({ submitLoading: false }, () => {
          message.success(response.status.message)
          onCloseModal()
          onRefresh()
        })
      }, (err) => {
        return this.setState({ submitLoading: false }, () => message.error(err.message))
      })
    })
  }

  onCancel = () => {
    const { onCloseModal, unmountGetDetail } = this.props
    return (
      onCloseModal(),
      unmountGetDetail()
    )
  }

  render() {
    const { getData: { data, loading } } = this.props
    if(loading){
      return <Loading height={200}/>
    }
    return (
      <React.Fragment>
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
          <Row gutter={16}>
            <Col sm={24} md={12}>
              <Form.Item label="Nama Skill" name="name" rules={[{ required: true, message: 'Nama Skill Tidak Boleh Kosong!' }]} initialValue={data.skillName}>
                <Input placeholder="Nama Skill" size="large" disabled />
              </Form.Item>
            </Col>
            <Col sm={24} md={12}>
              <Form.Item 
                label="Harga Satuan" 
                name="price"
                rules={[
                  { required: true, message: 'Harga Satuan Tidak Boleh Kosong!' },
                  { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                ]}
                initialValue={data.price}
              >
                <Input size="large" placeholder="Harga Satuan" addonBefore="Rp" style={{ width: '100%' }} />
              </Form.Item>
            </Col>
          </Row>

          <Divider />

          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button size="large" type="danger" onClick={() => this.onCancel()} style={{ marginRight: 10 }}>
              Batal
            </Button>
            <Button size="large" type="primary" htmlType="submit" className="register-form-button" loading={this.state.submitLoading}>
              Simpan
            </Button>
          </div>
          
        </Form>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.skill.detail
})

const mapDispatchToProps = {
  actioGetDetail    : detailSkill,
  actionSetEdit     : editSkill,
  unmountGetDetail  : unmountDetailSkill
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailSkill)
