import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Loading } from '../../../../components'
import { detailUser, editUser } from '../../../../redux/actions/user/userAction'
import { PageHeader, Menu, Typography, Form, Input, Card, Row, Col, Button, message } from 'antd'
import { SettingOutlined, UserOutlined } from '@ant-design/icons'

const { Text } = Typography

export class EditProfile extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       submitLoading: false
    }
  }

  componentDidMount() {
    const { actionGetDetail } = this.props
    return actionGetDetail();
  }
  
  onFinish = (values) => {
    const { actionSetEdit, getData: { data } } = this.props
    const newValue = values
    newValue.uid = data.uid
    return this.setState({ submitLoading: true }, () => {
      return actionSetEdit(newValue, async (response) => {
        return this.setState({ submitLoading: false }, () => message.success(response.status.message))
      }, (err) => {
        return this.setState({ submitLoading: false }, () => message.error(err.message))
      })
    })
  }
  
  render() {
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader 
          className="site-page-header" 
          title="UBAH PROFIL"
          ghost={true}
          extra={[
            <Menu key="1" selectedKeys="2" mode="horizontal" style={{ background: 'none' }}>
              <Menu.Item key="1" icon={<SettingOutlined />}>
                <Link to="/setting">
                  <Text>KEMAMPUAN</Text>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<UserOutlined />}>
                <Link to="/profile/edit">
                  <Text>UBAH PROFIL</Text>
                </Link>
              </Menu.Item>
            </Menu>,
          ]}
        />
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
          <Card>
            <Row gutter={16}>

              <Col sm={24} md={12}>
                <Form.Item label="Username" name="userName" rules={[{ required: true, message: 'Username Tidak Boleh Kosong!' }]} initialValue={data ? data.userName : null}>
                  <Input placeholder="Username" size="large" />
                </Form.Item>
              </Col>

              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Password Tidak Boleh Kosong!!' }]} initialValue={data ? data.password : null}>
                  <Input placeholder="Password" type="password" size="large" />
                </Form.Item>
              </Col>
            </Row>
          </Card>
          
          <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end', marginTop: 30 }}>
            <Form.Item>
              <Button size="large" type="primary" htmlType="submit" loading={this.state.submitLoading} className="register-form-button">
                Simpan
              </Button>
            </Form.Item>
          </Col>
        </Form>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  getData : state.user.detail
})

const mapDispatchToProps = {
  actionGetDetail : detailUser,
  actionSetEdit   : editUser
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)
