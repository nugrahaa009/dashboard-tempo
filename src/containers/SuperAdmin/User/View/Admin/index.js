import React, { Component } from 'react'
import { Loading } from '../../../../../components'
import { connect } from 'react-redux'
import { PageHeader, Form, Input, Select, Row, Col, Button, Divider, Modal, Card, message } from 'antd'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { editUser, detailUser, unmountDetailUser } from '../../../../../redux/actions/user/userAction'
const { confirm } = Modal

export class EditAdmin extends Component {

  componentDidMount() {
    const { actionGetDetail, match: { params } } = this.props
    return actionGetDetail(params.id)
  }

  onFinish = (values) => {
    const { actionSetEdit, match: { params } } = this.props
    const newValue = values
    newValue.uid = params.id
    confirm({
      title: "Apakah anda yakin ingin mengubah data admin?",
      icon: <ExclamationCircleOutlined />,
      okText: 'Simpan',
      okType: 'primary',
      cancelText: 'Cancel',
      onOk() {
        actionSetEdit(newValue, (response) => {
          message.success(response.status.message)
          window.history.back()
        }, (err) => message.error(err.message))
      }
    })
  }

  render() {
    const { getData: { data, loading } } = this.props
    if(loading){ return <Loading /> }
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="EDIT ADMIN" onBack={() => window.history.back()} />
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
          <Card title="Informasi Admin" style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={24} md={12}>
                <Form.Item label="Username" name="userName" rules={[{ required: true, message: 'Username Tidak Boleh Kosong!' }]} initialValue={data ? data.userName : null}>
                  <Input placeholder="Username" size="large" />
                </Form.Item>
              </Col>
              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Password Tidak Boleh Kosong!' }]} initialValue={data ? data.password : null}>
                  <Input placeholder="Password" type="password" size="large" />
                </Form.Item>
              </Col>
              <Divider />
              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item
                  label="NIP"
                  name="nip"
                  rules={[
                    { required: true, message: 'NIP Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                  ]}
                  initialValue={data ? data.nip : null}
                >
                  <Input placeholder="NIP" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Nama" name="fullName" rules={[{ required: true, message: 'Nama Tidak Boleh Kosong!' }]} initialValue={data ? data.userContact.fullName : null}>
                  <Input placeholder="Nama" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Jenis Kelamin" name="gender" rules={[{ required: true, message: 'Jenis Kelamin Tidak Boleh Kosong!' }]} initialValue={data ? data.userContact.gender === 'Male' ? 'Laki-Laki' : 'Perempuan' : null}>
                  <Select
                    placeholder="Jenis Kelamin"
                    size="large"
                    allowClear
                  >
                    <Select.Option value="Male">Laki-Laki</Select.Option>
                    <Select.Option value="Female">Perempuan</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item
                  label="Nomor Telepon"
                  name="phoneNumber"
                  rules={[
                    { required: true, message: 'Nomor Telepon Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                    { min: 10, message: 'Minimum 10 numbers' },
                    { max: 14, message: 'Maximum 14 numbers' },
                  ]}
                  initialValue={data ? data.phoneNumber : null}
                >
                  <Input placeholder="Nomor Telepon" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={24}>
                <Form.Item label="Alamat" name="address" initialValue={data ? data.userContact.address : null}>
                  <Input.TextArea placeholder="Alamat" size="large" rows={4} />
                </Form.Item>
              </Col>
            </Row>
          </Card>
          <Row gutter={16}>
            <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Form.Item>
                <Button className="register-form-button" size="large" type="danger" onClick={() => window.history.back()} style={{ marginRight: 10 }}>
                  Batal
                </Button>
                <Button size="large" type="primary" htmlType="submit" className="register-form-button">
                  Simpan
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData : state.user.detail
})

const mapDispatchToProps = {
  actionSetEdit     : editUser,
  actionGetDetail   : detailUser,
  unmountGetDetail  : unmountDetailUser
}

export default connect(mapStateToProps, mapDispatchToProps)(EditAdmin)
