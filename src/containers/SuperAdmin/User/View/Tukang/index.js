import React, { Component } from 'react'
import update from 'react-addons-update'
import { Loading } from '../../../../../components'
import { connect } from 'react-redux'
import { PageHeader, Card, Form, Input, Select, Row, Col, Button, Typography, Modal, message } from 'antd'
import { MinusCircleOutlined, PlusOutlined, InfoCircleTwoTone, ExclamationCircleOutlined } from '@ant-design/icons'
import { editUser, detailUser } from '../../../../../redux/actions/user/userAction'
import { listDataMaster, unmountDataMaster } from '../../../../../redux/actions/master/actionMaster'
const { Text } = Typography
const { confirm } = Modal

export class EditTukang extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      skill: [],
      loadingSkill: true,
    }
  }
  
  componentDidMount() {
    const { actionGetMaster, actionGetDetail, match: { params } } = this.props
    const type = [ 'master_skills' ]
    actionGetMaster(type, () => {
      return this.setState({ loadingFilter: false })
    })
    actionGetDetail(params.id, null, null, null, (response) => {
      this.setState({
        skill: response.body.skill.skill
      })
    })
  }

  onChange = (value) => {
    this.setState({
      skill: update(this.state.skill, {
        $push: [value]
      })
    })
  }

  removeState = (index) => {
    this.setState({
      skill: update(this.state.skill, {
        $splice: [[index, 1]]
      })
    })
  }

  onFinish = (values) => {
    const { actionSetEdit, match: { params } } = this.props
    const newValue = values
    newValue.uid = params.id
    confirm({
      title: "Apakah anda yakin ingin mengubah data tukang?",
      icon: <ExclamationCircleOutlined />,
      okText: 'Simpan',
      okType: 'primary',
      cancelText: 'Cancel',
      onOk() {
        actionSetEdit(newValue, (response) => {
          message.success(response.status.message)
          window.history.back()
        }, (err) => message.error(err.message))
      }
    })
  }

  render() {
    const { loadingFilter } = this.state
    const { getDataSkill, getData: { data, loading } } = this.props
    if(getDataSkill.loading || loading || loadingFilter){ return <Loading /> }
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="EDIT TUKANG" onBack={() => window.history.back()} />
        <Form 
          onFinish={this.onFinish} 
          name="normal_register" 
          layout="vertical"
          initialValues={{
            skills: data.skill.skill
          }}
        >
          <Card title="Informasi Tukang" style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item
                  label="NIP"
                  name="nip"
                  rules={[
                    { required: true, message: 'NIP Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                  ]}
                  initialValue={data ? data.nip : null}
                >
                  <Input placeholder="NIP" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Nama" name="fullName" rules={[{ required: true, message: 'Nama Tidak Boleh Kosong!' }]} initialValue={data ? data.userContact.fullName : null}>
                  <Input placeholder="Nama" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Jenis Kelamin" name="gender" rules={[{ required: true, message: 'Jenis Kelamin Tidak Boleh Kosong!' }]} initialValue={data ? data.userContact.gender === 'Male' ? 'Laki-Laki' : 'Perempuan' : null}>
                  <Select placeholder="Jenis Kelamin" size="large" allowClear>
                    <Select.Option value="Male">Laki-Laki</Select.Option>
                    <Select.Option value="Female">Perempuan</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item
                  label="Nomor Telepon"
                  name="phoneNumber"
                  rules={[
                    { required: true, message: 'Nomor Telepon Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                    { min: 10, message: 'Minimum 10 numbers' },
                    { max: 14, message: 'Maximum 14 numbers' },
                  ]}
                  initialValue={data ? data.phoneNumber : null}
                >
                  <Input placeholder="Nomor Telepon" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Status Pekerja" name="isStaff" rules={[{ required: true, message: 'Status Pekerja Tidak Boleh Kosong!' }]} initialValue={data ? data.isStaff : null}>
                  <Select placeholder="Pilih Status Pekerja" size="large">
                    <Select.Option value={1}>Staff</Select.Option>
                    <Select.Option value={0}>Non Staff</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={24} md={24}>
                <Form.Item label="Alamat" name="address" initialValue={data ? data.userContact.address : null}>
                  <Input.TextArea placeholder="Alamat" size="large" rows={4} />
                </Form.Item>
              </Col>
            </Row>
          </Card>
          <Card title="Pilih Kemampuan" style={{ marginBottom: 30 }}>
            <Col sm={24} md={24} style={{ marginBottom: 30 }}>
              <Text type="warning"><InfoCircleTwoTone twoToneColor="#FAAD15" /> Hanya bisa menambahkan kemampuan</Text>
            </Col>
            <Form.List name="skills">
              {(fields, { add, remove }) => (
                <>
                  {fields.map((field, index) => (
                    <Row gutter={16} key={field.fieldKey}>
                      <Col sm={24} md={23}>
                        <Form.Item 
                          {...field}
                          rules={[{ required: true, message: 'Pilih Kemampuan Tidak Boleh Kosong!' }]}
                        >
                          <Select
                            showSearch
                            style={{ width: '100%' }}
                            placeholder="Pilih Kemampuan"
                            size="large"
                            onChange={this.onChange}
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                            disabled={data.skill.skill.find((item, i) => i === index )}
                          >
                            {
                              getDataSkill.data && getDataSkill.data.master_skills.map((res, i) => {
                                const act = this.state.skill.includes(res.skillName || data.skill.skill);
                                return (
                                  <React.Fragment>
                                    {
                                      (!act) && 
                                      <Select.Option key={i} value={res.skillName}>
                                        {res.skillName}
                                      </Select.Option>
                                    }
                                  </React.Fragment>
                                )
                              })
                            }
                          </Select>
                        </Form.Item>
                      </Col>
                      {
                        !data.skill.skill.find((item, i) => i === index ) ?
                        <Col sm={1} md={1} style={{ paddingTop: 7 }}>
                          <MinusCircleOutlined disabled onClick={() => {
                            remove(field.name);
                            return this.removeState(index)
                          }} />
                        </Col>
                        :
                        null
                      }
                    </Row>
                  ))}
                  <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />} disabled={data.isEditProject}>
                      Tambah Form
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </Card>
          <Row gutter={16}>
            
            <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Form.Item>
                <Button className="register-form-button" size="large" type="danger" onClick={() => window.history.back()} style={{ marginRight: 10 }}>
                  Batal
                </Button>
                <Button className="register-form-button" type="primary" size="large" htmlType="submit">
                  Simpan
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountDataMaster } = this.props
    return unmountDataMaster()
  }
}

const mapStateToProps = (state) => ({
  getData       : state.user.detail,
  getDataSkill  : state.master

})

const mapDispatchToProps = {
  actionSetEdit     : editUser,
  actionGetDetail   : detailUser,
  actionGetMaster   : listDataMaster,
  unmountDataMaster : unmountDataMaster
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTukang)
