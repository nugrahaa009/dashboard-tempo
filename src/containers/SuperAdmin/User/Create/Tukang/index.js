import React, { Component } from 'react'
import update from 'react-addons-update'
import { Loading } from '../../../../../components'
import { connect } from 'react-redux'
import { PageHeader, Form, Input, Select, Row, Col, Card, Button, message } from 'antd'
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'
import { createUser } from '../../../../../redux/actions/user/userAction'
import { listDataMaster, unmountDataMaster } from '../../../../../redux/actions/master/actionMaster'

export class CreateTukang extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      skill: [],
      submitLoading: false
    }
  }
  
  componentDidMount() {
    const { actionGetMaster } = this.props
    const type = [ 'master_skills' ]
    return actionGetMaster(type)
  }

  onChange = (value) => {
    this.setState({
      skill: update(this.state.skill, {
        $push: [value]
      })
    })
  }

  removeState = (index) => {
    this.setState({
      skill: update(this.state.skill, {
        $splice: [[index, 1]]
      })
    })
  }

  onFinish = (values) => {
    const { actionSetCreate } = this.props
    const newValue = values
    newValue.type = 3
    this.setState({ submitLoading: true })
    return actionSetCreate(newValue, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        message.success(response.status.message)
        window.history.back()
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
     
    })
  }
  
  render() {
    const { getData: { data, loading } } = this.props
    if(loading){ return <Loading /> }
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="BUAT TUKANG" onBack={() => window.history.back()} />
        <Form 
          onFinish={this.onFinish} 
          name="normal_register" 
          layout="vertical"
          initialValues={{
            skill: [""]
          }}
        >
          <Card title="Informasi Tukang" style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={24} md={12}>
                <Form.Item
                  label="NIP"
                  name="nip"
                  rules={[
                    { required: true, message: 'NIP Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                  ]}>
                  <Input placeholder="NIP" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Nama" name="fullName" rules={[{ required: true, message: 'Nama Tidak Boleh Kosong!' }]}>
                  <Input placeholder="Nama" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Jenis Kelamin" name="gender" rules={[{ required: true, message: 'Jenis Kelamin Tidak Boleh Kosong!' }]}>
                  <Select placeholder="Jenis Kelamin" size="large" allowClear>
                    <Select.Option value="Male">Laki-Laki</Select.Option>
                    <Select.Option value="Female">Perempuan</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item
                  label="Nomor Telepon"
                  name="phoneNumber"
                  rules={[
                    { required: true, message: 'Nomor Telepon Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                    { min: 10, message: 'Minimum 10 numbers' },
                    { max: 14, message: 'Maximum 14 numbers' },
                  ]}>
                  <Input placeholder="Nomor Telepon" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Status Pekerja" name="isStaff" rules={[{ required: true, message: 'Status Pekerja Tidak Boleh Kosong!' }]}>
                  <Select placeholder="Pilih Status Pekerja" size="large">
                    <Select.Option value={1}>Staff</Select.Option>
                    <Select.Option value={0}>Non Staff</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col sm={24} md={24}>
                <Form.Item label="Alamat" name="address">
                  <Input.TextArea placeholder="Alamat" size="large" rows={4} />
                </Form.Item>
              </Col>
            </Row>
          </Card>
          <Card title="Pilih Kemampuan" style={{ marginBottom: 30 }}>
            <Form.List name="skills">
              {(fields, { add, remove }) => (
                <>
                  {fields.map((field, index) => (
                    <Row gutter={16} key={field.fieldKey}>
                      <Col sm={24} md={23}>
                        <Form.Item 
                          {...field}
                          rules={[{ required: true, message: 'Pilih Kemampuan Tidak Boleh Kosong!' }]}
                        >
                          <Select
                            showSearch
                            style={{ width: '100%' }}
                            placeholder="Pilih Kemampuan"
                            size="large"
                            onChange={this.onChange}
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {
                              data && data.master_skills.map((res, i) => {
                                const act = this.state.skill.includes(res.skillName);
                                return (
                                  <React.Fragment>
                                    {
                                      !act && 
                                      <Select.Option key={i} value={res.skillName}>
                                        {res.skillName}
                                      </Select.Option>
                                    }
                                  </React.Fragment>
                                )
                              })
                            }
                          </Select>
                        </Form.Item>
                      </Col>
                      <Col sm={1} md={1} style={{ paddingTop: 7 }}>
                        <MinusCircleOutlined onClick={() => {
                          remove(field.name);
                          return this.removeState(index)
                        }} />
                      </Col>
                    </Row>
                  ))}
                  <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                      Tambah Form
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </Card>
          <Row gutter={16}>
            <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Form.Item>
                <Button className="register-form-button" size="large" type="danger" onClick={() => window.history.back()} style={{ marginRight: 10 }}>
                  Batal
                </Button>
                <Button className="register-form-button" size="large" type="primary" htmlType="submit" loading={this.state.submitLoading}>
                  Simpan
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountDataMaster } = this.props
    return unmountDataMaster()
  }
}

const mapStateToProps = (state) => ({
  getData: state.master
})

const mapDispatchToProps = {
  actionSetCreate   : createUser,
  actionGetMaster   : listDataMaster,
  unmountDataMaster : unmountDataMaster
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTukang)
