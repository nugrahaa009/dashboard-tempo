import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PageHeader, Form, Input, Select, Row, Col, Card, Button, message, Divider } from 'antd'
import { createUser } from '../../../../../redux/actions/user/userAction'

export class CreateStakeholder extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       submitLoading: false
    }
  }

  onFinish = (values) => {
    const { actionSetCreate } = this.props
    const newValue = values
    newValue.type = 4
    this.setState({ submitLoading: true })
    return actionSetCreate(newValue, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        message.success(response.status.message)
        window.history.back()
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }

  render() {
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="BUAT STAKEHOLDER" onBack={() => window.history.back()} />
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
          <Card title="BUAT STAKEHOLDER">
            <Row gutter={16}>
              <Col sm={24} md={12}>
                <Form.Item label="Username" name="userName" rules={[{ required: true, message: 'Username Tidak Boleh Kosong!' }]}>
                  <Input placeholder="Username" size="large" />
                </Form.Item>
              </Col>

              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Password Tidak Boleh Kosong!' }]}>
                  <Input placeholder="Password" type="password" size="large" />
                </Form.Item>
              </Col>

              <Divider />

              <Col sm={24} md={24}>
                <Form.Item label="Nama" name="fullName" rules={[{ required: true, message: 'Nama Tidak Boleh Kosong!' }]}>
                  <Input placeholder="Nama" size="large" />
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item label="Jenis Kelamin" name="gender" rules={[{ required: true, message: 'Jenis Kelamin Tidak Boleh Kosong!' }]}>
                  <Select
                    placeholder="Jenis Kelamin"
                    size="large"
                    allowClear
                  >
                    <Select.Option value="Male">Laki-Laki</Select.Option>
                    <Select.Option value="Female">Perempuan</Select.Option>
                  </Select>
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item
                  label="Nomor Telepon"
                  name="phoneNumber"
                  rules={[
                    { required: true, message: 'Nomor Telepon Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                    { min: 10, message: 'Minimum 10 numbers' },
                    { max: 14, message: 'Maximum 14 numbers' },
                  ]}>
                  <Input placeholder="Nomor Telepon" size="large" />
                </Form.Item>
              </Col>

              <Col sm={24} md={24}>
                <Form.Item label="Alamat" name="address">
                  <Input.TextArea placeholder="Alamat" size="large"  rows={4} />
                </Form.Item>
              </Col>

             
              <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Form.Item>
                  <Button className="register-form-button" size="large" type="danger" onClick={() => window.history.back()} style={{ marginRight: 10 }}>
                    Batal
                  </Button>
                  <Button className="register-form-button" size="large" type="primary" htmlType="submit" loading={this.state.submitLoading}>
                    Simpan
                  </Button>
                </Form.Item>
              </Col>
              
            </Row>
          </Card>
        </Form>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  actionSetCreate   : createUser,
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateStakeholder)
