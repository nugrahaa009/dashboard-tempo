import React, { Component } from 'react'
import { connect } from 'react-redux'
import { columns } from './columns'
import { FilterTable } from '../../../components'
import { Form, Select, Button, Col, PageHeader, Row, Table, Modal, message } from 'antd'
import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons'
import { deleteUser, listUser, unmountDataUser } from '../../../redux/actions/user/userAction'
const qs = require('qs')
const { confirm } = Modal

export class User extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      userType: 0,
      filter: {
        search: null,
        type: 'userType',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true });
    const filter = {
      "type": null,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(current.page, this.state.userType, this.state.perPage, filter)
      })
    }else{
      return actionGetData(1, this.state.userType, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(e.current, this.state.userType, e.pageSize, this.state.filter);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { actionGetData } = this.props
    return this.setState({ 
      'current': 1, 
      'userType': e.userType,  
      "filter": {
        'search': e.search,
        'orderBy': e.orderBy,
        'type': e.type
      }, 
    }, () => {
      return actionGetData(this.state.current, this.state.userType, this.state.perPage, this.state.filter);
    })
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  onCancel = () => {
    this.setState({ visible: false })
  }

  deleteConfirm = (id) => {
    const { current } = this.state
    const { actionSetDelete, actionGetData } = this.props
    const payload = {
      "uid": id
    }
    confirm({
      title: 'Apa anda yakin ingin menghapus akun ini?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Hapus',
      okType: 'danger',
      cancelText: 'Cancel',
      onOk() {
        actionSetDelete(payload, (response) => {
          message.success(response.status.message)
          return actionGetData(current, null)
        }, (err) => {
          return message.error(err.message)
        })
      },
    });
  }

  onEdit = (type, uid) => {
    const { history } = this.props
    switch(type){
      case 2:
        return history.push(`/user/edit/admin/${uid}`)
      case 3:
        return history.push(`/user/edit/tukang/${uid}`)
      case 4:
        return history.push(`/user/edit/stakeholder/${uid}`)
      case 5:
        return history.push(`/user/edit/quality-control/${uid}`)
      case 6:
        return history.push(`/user/edit/fitting/${uid}`)
      default:
        return null
    }
  }


  onCreate = (values) => {
    const { history } = this.props
    this.setState({ visible: false })
    switch(values.role){
      case 'tukang':
        return history.push(`/user/create/tukang`)
      case 'admin':
        return history.push(`/user/create/admin`)
      case 'stakeholder':
        return history.push(`/user/create/stakeholder`)
      case 'qualityControl':
        return history.push(`/user/create/quality-control`)
      case 'fitting':
        return history.push(`/user/create/fitting`)
      default:
        return null
    }
  }

  render() {
    const { getData: { data, loading, pagination } } = this.props
    const { current, perPage } = this.state

    const defaultStatusFilter = [
      { value: 'fullName', name: 'Nama Lengkap' },
    ]
    
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="PENGGUNA" />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterTable 
              fase1={null}
              fase2={{ key: 'type', value: defaultStatusFilter}}
              fase3
              onSearch={this.onSearch}
            />
          </Col>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Button size="large" type="dashed" onClick={this.showModal} icon={<PlusOutlined />} block>
              Tambah Pengguna
            </Button>
            <Modal
              visible={this.state.visible}
              title="Pilih Peran"
              closable={false}
              footer={null}
            >
              <Form onFinish={this.onCreate} name="normal_register" layout="vertical">
                <Row gutter={16}>
                  <Col span={24}>
                    <Form.Item name="role" rules={[{ required: true, message: 'Peran Tidak Boleh Kosong!' }]}>
                      <Select
                        placeholder="Pilih Peran"
                        size="large"
                        allowClear
                      >
                        <Select.Option value="tukang">Tukang</Select.Option>
                        <Select.Option value="admin">Admin</Select.Option>
                        <Select.Option value="stakeholder">Stakeholder</Select.Option>
                        <Select.Option value="qualityControl">Quality Control</Select.Option>
                        <Select.Option value="fitting">Fitting</Select.Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button  onClick={this.onCancel} size="large" type="default" className="register-form-button" style={{ marginRight: 10 }}>
                      Batal
                    </Button>
                    <Button type="primary" size="large" htmlType="submit" className="register-form-button">
                      Lanjutkan
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Modal>
          </Col>  
          <Col sm={24} md={24}>
            <Table 
              rowKey={(i) => i.uid}
              loading={loading} 
              dataSource={data} 
              columns={columns(current, perPage, this.onEdit, this.deleteConfirm)} 
              pagination={pagination} 
              onChange={this.pagination} 
            />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData: state.user.list
})

const mapDispatchToProps = {
  actionGetData   : listUser,
  actionSetDelete : deleteUser,
  unmountGetData  : unmountDataUser
}

export default connect(mapStateToProps, mapDispatchToProps)(User)
