import React, { Component } from 'react'
import NumberFormat from 'react-number-format'
import { FilterBillingHistory, Loading } from '../../../../components'
import { columns } from './columns'
import { connect } from 'react-redux'
import { PageHeader, Table, Row, Col, Typography, Card } from 'antd'
import { detailUser, unmountDetailUser } from '../../../../redux/actions/user/userAction'
const qs = require('qs')
const { Text, Title } = Typography

export class BillingPayment extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      filter: {
        search: null,
        type: 'date',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetDetail, match: { params }, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetDetail(params.id, current.page, this.state.perPage, filter)
      })
    }else{
      return actionGetDetail(params.id, 1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetDetail, match: { params } } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetDetail(params.id, e.current);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { history, actionGetDetail, match: { params } } = this.props
    const payload = {
      type: 'date',
      search: e.date !== undefined ? e.date : null,
      orderBy: 'desc'
    }
    return this.setState({ "curent": 1, "filter": payload  }, () => {
      history.push()
      window.scrollTo(0, 0);
      return actionGetDetail(params.id, this.state.current, this.state.perPage, this.state.filter)
    })
  }
  
  render() {
    const { current, perPage } = this.state
    const { getData: { loading, data, pagination } } = this.props
    if(loading){
      return <Loading />
    }
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="RIWAYAT PENAGIHAN" onBack={() => this.props.history.push('/user')} />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Card title="Data Pengguna">
              <Row gutter={16}>
                <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                  <Title level={5}>Nama:</Title>
                  <Text>{data.userContact.fullName}</Text>
                </Col>
                <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                  <Title level={5}>NIP:</Title>
                  <Text>{data.nip}</Text>
                </Col>
                <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                  <Title level={5}>Nomor Telepon:</Title>
                  <Text>{data.phoneNumber}</Text>
                </Col>
                <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                  <Title level={5}>Status Pekerja:</Title>
                  {
                    data.isStaff ? 
                    <Text>Staff</Text>
                    :
                    <Text>Non Staff</Text>
                  }
                </Col>
              </Row>
            </Card>
          </Col>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterBillingHistory onSearch={this.onSearch} />
          </Col>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Table 
              rowKey={(i) => i.id}
              dataSource={data.listPayment.data} 
              columns={columns(current, perPage)} 
              loading={loading} 
              pagination={pagination} 
              onChange={this.pagination} 
              summary={pageData => {
                let _totalPrice = 0;

                pageData.forEach(({ total }) => {
                  _totalPrice += total;
                });
                return (
                  <>
                    <Table.Summary.Row style={{ textAlign: 'center' }}>
                      <Table.Summary.Cell />
                      <Table.Summary.Cell>
                        <Title level={3}>Jumlah</Title>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell />
                      <Table.Summary.Cell />
                      <Table.Summary.Cell />
                      <Table.Summary.Cell />
                      <Table.Summary.Cell>
                        <NumberFormat 
                          value={_totalPrice ? _totalPrice : 0} 
                          displayType={'text'} 
                          thousandSeparator={true} 
                          prefix={'Rp '} 
                          renderText={value => <Title level={3}>{value}</Title>} 
                        />
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                  </>
                );
              }}
            />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  getData: state.user.detail
})

const mapDispatchToProps = {
  actionGetDetail : detailUser,
  unmountDetail   : unmountDetailUser
}

export default connect(mapStateToProps, mapDispatchToProps)(BillingPayment)
