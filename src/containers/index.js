import Login from './Login'
import Dashboard from './Dashboard'
import NotFound from './404'
import User from './SuperAdmin/User'
import BillingPayment from './SuperAdmin/User/BillingPayment'
import CreateAdmin from './SuperAdmin/User/Create/Admin'
import CreateTukang from './SuperAdmin/User/Create/Tukang'
import CreateStakeholder from './SuperAdmin/User/Create/Stakeholder'
import CreateQualityControl from './SuperAdmin/User/Create/QualityControl'
import CreateFitting from './SuperAdmin/User/Create/Fitting'
import EditAdmin from './SuperAdmin/User/View/Admin'
import EditTukang from './SuperAdmin/User/View/Tukang'
import EditStakeholder from './SuperAdmin/User/View/Stakeholder'
import EditQualityControl from './SuperAdmin/User/View/QualityControl'
import EditFitting from './SuperAdmin/User/View/Fitting'
import Payment from './SuperAdmin/Payment/Payment'
import DetailPayment from './SuperAdmin/Payment/Payment/Detail'
import History from './SuperAdmin/Payment/History'
import DetailHistory from './SuperAdmin/Payment/History/Detail'
import Project from './SuperAdmin/Project'
import CreateProject from './SuperAdmin/Project/Create'
import DetailPlanning from './SuperAdmin/Project/Detail/Planning'
import EditPlanning from './SuperAdmin/Project/Edit'
import ListProject from './SuperAdmin/Project/Detail/Project'
import DetailProjectCredential from './SuperAdmin/Project/Detail/Project/DetailCredential'
import DetailProjectSize from './SuperAdmin/Project/Detail/Project/DetailSize'
import DetailProjectSkill from './SuperAdmin/Project/Detail/Project/DetailSkill'
import Skill from './SuperAdmin/Setting/Skill'
import CreateSkill from './SuperAdmin/Setting/Skill/Create'
import DetailSkill from './SuperAdmin/Setting/Skill/Detail'
import EditProfile from './SuperAdmin/Setting/EditProfile'

import Admin from './Admin/Project'
import AdminCreateProject from './Admin/Project/Create'
import AdminEditProject from './Admin/Project/Edit'
import AdminDetailPlanning from './Admin/Project/Detail/Planning'
import AdminListProject from './Admin/Project/Detail/Project'
import AdminProfileEdit from './Admin/Setting/EditProfile'

import Stakeholder from './Stakeholder/Dashboard'
import StakeholderUser from './Stakeholder/User'
import StakeholderDetailAdmin from './Stakeholder/User/View/Admin'
import StakeholderDetailTukang from './Stakeholder/User/View/Tukang'
import StakeholderDetailStakeholder from './Stakeholder/User/View/Stakeholder'
import StakeholderDetailQualityControl from './Stakeholder/User/View/QualityControl'
import StakeholderDetailFitting from './Stakeholder/User/View/Fitting'
import StakeholderBillingPayment from './Stakeholder/User/BillingPayment'
import StakeholderHistory from './Stakeholder/Payment/History'
import StakeholderDetailHistory from './Stakeholder/Payment/History/Detail'
import StakeholderPayment from './Stakeholder/Payment/Payment'
import StakeholderDetailPayment from './Stakeholder/Payment/Payment/Detail'
import StakeholderProject from './Stakeholder/Project'
import StakeholderDetailPlanning from './Stakeholder/Project/Detail/Planning'
import StakeholderListProject from './Stakeholder/Project/Detail/Project'
import StakeholderEditProfile from './Stakeholder/Setting/EditProfile'

export {
  Login,
  Dashboard,
  NotFound,
  User,
  CreateTukang,
  CreateAdmin,
  CreateFitting,
  CreateStakeholder,
  CreateQualityControl,
  EditAdmin,
  EditTukang,
  EditStakeholder,
  EditQualityControl,
  EditFitting,
  BillingPayment,
  EditProfile,
  Payment,
  DetailPayment,
  History,
  DetailHistory,
  Project,
  CreateProject,
  DetailPlanning,
  EditPlanning,
  ListProject,
  DetailProjectCredential,
  DetailProjectSize,
  DetailProjectSkill,
  Skill,
  CreateSkill,
  DetailSkill,

  Admin,
  AdminCreateProject,
  AdminEditProject,
  AdminDetailPlanning,
  AdminListProject,
  AdminProfileEdit,

  Stakeholder,
  StakeholderUser,
  StakeholderDetailAdmin,
  StakeholderDetailTukang,
  StakeholderDetailStakeholder,
  StakeholderDetailQualityControl,
  StakeholderDetailFitting,
  StakeholderBillingPayment,
  StakeholderHistory,
  StakeholderDetailHistory,
  StakeholderPayment,
  StakeholderDetailPayment,
  StakeholderProject,
  StakeholderDetailPlanning,
  StakeholderListProject,
  StakeholderEditProfile
}