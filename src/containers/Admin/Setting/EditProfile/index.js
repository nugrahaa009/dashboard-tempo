import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../components'
import { detailUser, editUser } from '../../../../redux/actions/user/userAction'
import { PageHeader, Form, Input, Card, Row, Col, Button, message, Select, Divider } from 'antd'


export class AdminProfileEdit extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       submitLoading: false
    }
  }

  componentDidMount() {
    const { actionGetDetail } = this.props
    return actionGetDetail();
  }
  
  onFinish = (values) => {
    const { actionSetEdit, getData: { data } } = this.props
    const newValue = values
    newValue.uid = data.uid
    return this.setState({ submitLoading: true }, () => {
      return actionSetEdit(newValue, async (response) => {
        return this.setState({ submitLoading: false }, () => message.success(response.status.message))
      }, (err) => {
        return this.setState({ submitLoading: false }, () => message.error(err.message))
      })
    })
  }
  
  render() {
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="UBAH PROFIL" />
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
        <Card title="UBAH ADMIN">
            <Row gutter={16}>
              <Col sm={24} md={12}>
                <Form.Item label="Username" name="userName" rules={[{ required: true, message: 'Username Tidak Boleh Kosong!' }]} initialValue={data ? data.userName : null}>
                  <Input placeholder="Username" size="large" />
                </Form.Item>
              </Col>

              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item label="Password" name="password" rules={[{ required: true, message: 'Password Tidak Boleh Kosong!' }]} initialValue={data ? data.password : null}>
                  <Input placeholder="Password" type="password" size="large" />
                </Form.Item>
              </Col>

              <Divider />

              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item
                  label="NIP"
                  name="nip"
                  rules={[
                    { required: true, message: 'NIP Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                  ]}
                  initialValue={data ? data.nip : null}
                >
                  <Input placeholder="NIP" size="large" />
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item label="Nama" name="fullName" rules={[{ required: true, message: 'Nama Tidak Boleh Kosong!' }]} initialValue={data ? data.userContact.fullName : null}>
                  <Input placeholder="Nama" size="large" />
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item label="Jenis Kelamin" name="gender" rules={[{ required: true, message: 'Jenis Kelamin Tidak Boleh Kosong!' }]} initialValue={data ? data.userContact.gender === 'Male' ? 'Laki-Laki' : 'Perempuan' : null}>
                  <Select
                    placeholder="Jenis Kelamin"
                    size="large"
                    allowClear
                  >
                    <Select.Option value="Male">Laki-Laki</Select.Option>
                    <Select.Option value="Female">Perempuan</Select.Option>
                  </Select>
                </Form.Item>
              </Col>

              <Col sm={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item
                  label="Nomor Telepon"
                  name="phoneNumber"
                  rules={[
                    { required: true, message: 'Nomor Telepon Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                    { min: 10, message: 'Minimum 10 numbers' },
                    { max: 14, message: 'Maximum 14 numbers' },
                  ]}
                  initialValue={data ? data.phoneNumber : null}
                >
                  <Input placeholder="Nomor Telepon" size="large" />
                </Form.Item>
              </Col>

              <Col sm={24} md={24}>
                <Form.Item label="Alamat" name="address" initialValue={data ? data.userContact.address : null}>
                  <Input.TextArea placeholder="Alamat" size="large" rows={4} />
                </Form.Item>
              </Col>

              <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Form.Item>
                  <Button className="register-form-button" size="large" type="danger" onClick={() => window.history.back()} style={{ marginRight: 10 }}>
                    Batal
                  </Button>
                  <Button size="large" type="primary" htmlType="submit" loading={this.state.submitLoading} className="register-form-button">
                    Simpan
                  </Button>
                </Form.Item>
              </Col>

            </Row>
          </Card>
        </Form>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  getData : state.user.detail
})

const mapDispatchToProps = {
  actionGetDetail : detailUser,
  actionSetEdit   : editUser
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminProfileEdit)
