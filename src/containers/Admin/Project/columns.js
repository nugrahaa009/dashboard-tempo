import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { Button, Badge, Typography, Space } from 'antd'
import { FileSearchOutlined, DeleteOutlined } from '@ant-design/icons'
import { number } from '../../../helper/pagination'

const { Text } = Typography

export const columns = (current, perPage, deleteConfirm) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 80,
      render: (data, text, index) => number(index, current, perPage)
    },
    {
      title: 'Nama Order',
      render: (record) => (
        <Text>
          {record ? record.name : '-'} 
        </Text>
      )
    },
    {
      title: 'Jumlah Pesanan',
      align: 'center',
      render: (record) => (
        <Text>
          {record ? `${record.total} pcs` : '-'} 
        </Text>
      )
    },
    {
      title: 'DOD',
      align: 'center',
      render: (record) => (
        <Text>
          {record ? moment(record.deadline).format('LL') : '-'} 
        </Text>
      )
    },
    {
      title: 'Status',
      align: 'center',
      render: (record) => (
        <Text>
          {
            record ? record.isHold ? 
            <Text type="danger"> <Badge status="error" /> Tertunda </Text> 
            : 
            <Text type="warning"> <Badge status="warning" /> Sedang Berlangsung </Text> 
            : 
            '-'
          } 
        </Text>
      )
    },
    {
      title: 'Jumlah Scan',
      align: 'center',
      render: (record) => (
        <React.Fragment>
          {
            record ? record.count < record.totalProject ?
            <Text type="danger">
              {record.count}/{record.totalProject}
            </Text>
            :
            <Text type="success">
              {record.count}/{record.totalProject}
            </Text>
            :
            '-'
          }
        </React.Fragment>
      )
    },
    {
      title: 'Catatan',
      align: 'center',
      render: (record) => (
        <Text>
          {record ? record.totalComment : '-'} 
        </Text>
      )
    },
    {
      title: '',
      align: 'right',
      render: (record) => (
        <Space>
          <Link to={`/admin/project/planning/detail/${record.uid}`}>
            <Button type="primary" shape="circle" icon={<FileSearchOutlined />} disabled={record.isHold === 1 ? true : false} />
          </Link>
          <Button type="danger" shape="circle" icon={<DeleteOutlined />} onClick={() => deleteConfirm(record.uid)} disabled={record.isHold === 1 ? true : false} />
        </Space>
      )
    },
  ]
}