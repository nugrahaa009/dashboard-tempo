import React, { Component } from 'react'
import moment from 'moment'
import update from 'react-addons-update'
import { connect } from 'react-redux'
import { Loading } from '../../../../components'
import { PageHeader, Form, Input, Row, Col, Card, Button, Select, Upload, Typography, DatePicker, message, Space } from 'antd'
import { UploadOutlined, MinusCircleOutlined, PlusOutlined, PaperClipOutlined } from '@ant-design/icons'
import { listDataMaster } from '../../../../redux/actions/master/actionMaster'
import { createProject, importCSV } from '../../../../redux/actions/project/projectAction'

const { Text } = Typography

export class CreateProject extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       submitLoading: false,
       fileList: null,
       fileName: null,
       skill: []
    }
  }

  componentDidMount() {
    const { actionGetMaster } = this.props
    const type = [ 'master_skills' ]
    return actionGetMaster(type)
  }

  handleUpload(){
    return {
      showUploadList: false,
      withCredentials: true,
      accept:".csv",
      beforeUpload: file => {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.setState({ 
            fileList: file, 
            fileName: file.name
          })
        }
        return false;
      }
    }
  }

  onChange = (value) => {
    this.setState({
      skill: update(this.state.skill, {
        $push: [value]
      })
    })
  }

  removeState = (index) => {
    this.setState({
      skill: update(this.state.skill, {
        $splice: [[index, 1]]
      })
    })
  }

  onFinish = (values) => {
    const { fileList } = this.state
    const { actionSetCerate, history } = this.props
    const newValue = values
    newValue.deadline = moment(values.deadline).format('YYYY-MM-DD')
    newValue.size = JSON.stringify(values.size)
    newValue.skill = JSON.stringify(values.skill)
    newValue.notes = values.notes !== undefined ? values.notes : null
    this.setState({ submitLoading: true })
    const payload = {
      file: fileList,
      ...newValue
    }
    return actionSetCerate(payload, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        message.success(response.status.message)
        history.push('/admin')
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }

  render() {
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="BUAT PROJECT" onBack={() => this.props.history.push('/admin')} />
        <Form 
          onFinish={this.onFinish} 
          name="normal_register" 
          layout="vertical"
          initialValues={{
            size: [
              { typeSize: "" },
            ],
            skill: [""]
          }}
        >
          <Card title="Informasi Planning Project" style={{ marginBottom: 30 }}>
            <Row gutter={16}>

              <Col sm={24} md={12}>
                <Form.Item label="Nama Proyek" name="name" rules={[{ required: true, message: 'Nama Proyek Tidak Boleh Kosong!' }]}>
                  <Input placeholder="Nama Proyek" size="large" />
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item label="Nama Perusahaan" name="companyName" rules={[{ required: true, message: 'Nama Perusahaan Tidak Boleh Kosong!' }]}>
                  <Input placeholder="Nama Perusahaan" size="large" />
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item 
                  label="Jumlah Pesanan" 
                  name="total"
                  rules={[
                    { required: true, message: 'Jumlah Pesanan Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                  ]}
                >
                  <Input size="large" placeholder="Jumlah Pesanan" style={{ width: '100%' }} />
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item label="DOD" name="deadline" rules={[{ required: true, message: 'DOD Tidak Boleh Kosong!' }]}>
                  <DatePicker 
                    placeholder="Pilih Tanggal" 
                    style={{ width: '100%' }} 
                    size="large" 
                    disabledDate={(current) => {
                      return current && current < moment().add(-1, "days");
                    }}
                  />
                </Form.Item>
              </Col>

              <Col sm={24} md={12}>
                <Form.Item label="Unggah CSV" rules={[{ required: true, message: 'Unggah CSV Tidak Boleh Kosong!' }]}>
                  <Space>
                    <Upload {...this.handleUpload()}>
                      <Button type="primary" icon={<UploadOutlined />}>Unggah CSV</Button>
                    </Upload>
                    { this.state.fileName &&
                      <Text>
                        <PaperClipOutlined /> {this.state.fileName}
                      </Text>
                    }
                  </Space>
                </Form.Item>
              </Col>

              <Col sm={24} md={24}>
                <Form.Item label="Keterangan" name="notes">
                  <Input.TextArea placeholder="Keterangan" size="large" rows={4} />
                </Form.Item>
              </Col>

            </Row>
          </Card>

          <Card title="Pilih Ukuran" style={{ marginBottom: 30 }}>
            <Form.List name="size">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(field => (
                    <Row gutter={16} key={field.fieldKey}>
                      <Col sm={24} md={8}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'typeSize']}
                          fieldKey={[field.fieldKey, 'typeSize']}
                          rules={[{ required: true, message: 'Jenis Ukuran Tidak Boleh Kosong!' }]}
                        >
                          <Input placeholder="Jenis Ukuran" size="large" />
                        </Form.Item>
                      </Col>
                      <Col sm={24} md={16}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'value']}
                          fieldKey={[field.fieldKey, 'value']}
                          rules={[{ required: true, message: 'Proses Tidak Boleh Kosong!' }]}
                          extra="Note: Silahkan menggunakan (, atau enter) untuk membuat lebih dari 1"
                        >
                          <Select mode="tags" tokenSeparators={[',']} placeholder="Proses" size="large" dropdownStyle={{ display: 'none' }} />
                        </Form.Item>
                      </Col>
                    </Row>
                  ))}
                  
                </>
              )}
            </Form.List>
          </Card>

          <Card title="Pilih Kemampuan" style={{ marginBottom: 30 }}>
            <Form.List name="skill">
              {(fields, { add, remove }) => (
                <>
                  {fields.map((field, index) => (
                    <Row gutter={16} key={field.fieldKey}>
                      <Col sm={24} md={23}>
                        <Form.Item 
                          {...field}
                          rules={[{ required: true, message: 'Pilih Kemampuan Tidak Boleh Kosong!' }]}
                        >
                          <Select
                            showSearch
                            style={{ width: '100%' }}
                            placeholder="Pilih Kemampuan"
                            size="large"
                            onChange={this.onChange}
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {
                              data && data.master_skills.map((res, i) => {
                                const act = this.state.skill.includes(res.skillName);
                                return (
                                  <React.Fragment>
                                    {
                                      !act && 
                                      <Select.Option key={i} value={res.skillName}>
                                        {res.skillName}
                                      </Select.Option>
                                    }
                                  </React.Fragment>
                                )
                              })
                            }
                          </Select>
                        </Form.Item>
                      </Col>
                      <Col sm={1} md={1} style={{ paddingTop: 7 }}>
                        <MinusCircleOutlined onClick={() => {
                          remove(field.name);
                          return this.removeState(index)
                        }} />
                      </Col>
                    </Row>
                  ))}
                  <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                      Tambah Form
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </Card>

          <Row gutter={16}>
            <Col sm={{ span: 24 }} md={{ span: 24 }} style={{ marginBottom: 30 }}>
              <Button loading={this.state.submitLoading} size="large" type="primary" htmlType="submit" className="register-form-button" block>
                BUAT PROJECT
              </Button>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  getData: state.master
})

const mapDispatchToProps = {
  actionGetMaster   : listDataMaster,
  actionSetImport   : importCSV,
  actionSetCerate   : createProject,
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateProject)
