import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../../../components'
import { Col, Row, Steps, Button, Divider } from 'antd'
import { detailProject, unmountDetailProject } from '../../../../../../redux/actions/project/projectAction'

const { Step } = Steps

export class DetailProses extends Component {

  componentDidMount() {
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid, 'process')
  }
  
  onCancel = () => {
    const { onCancel } = this.props
    return onCancel()
  }
  
  render() {
    const { getData: { data, loading } } = this.props
    
    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <Row gutter={16}>
          <Col sm={24} md={24}>
            <Steps direction="vertical" current={data.currentStatus}>
              {
                data.historyProcess.map((item, i) => (
                  <Step 
                    key={i} 
                    title={item.skill} 
                    description={
                      <React.Fragment>
                        {item.qcName ? `Nama QC: ${item.qcName}` : ''}
                        <br />
                        {item.user ? `Tukang: ${item.user}` : ''}
                      </React.Fragment>
                    }
                  />
                ))
              }
            </Steps>
          </Col>
        </Row>

        <Divider />

        <div style={{ marginBottom: 15, display: 'flex', justifyContent: 'flex-end' }}>
          <Button size="large" type="danger" onClick={() => this.onCancel()}>
            Kembali
          </Button>
        </div>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProses)