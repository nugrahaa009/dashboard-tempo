import React, { Component } from 'react'
import DetailProjectSkill from './DetailSkill'
import DetailCredential from './DetailCredential'
import DetailSize from './DetailSize'
import DetailProses from './DetailProses'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { columns } from './columns'
import { FilterProject } from '../../../../../components'
import { PageHeader, Menu, Typography, Row, Col, Table, Modal, message, Button } from 'antd'
import { DatabaseOutlined, TableOutlined, ExclamationCircleOutlined, PrinterOutlined } from '@ant-design/icons'
import { deleteListProject, detailProject, listProject, printLoketing, unmountDataProject } from '../../../../../redux/actions/project/projectAction'
const qs = require('qs')
const { Text } = Typography
const { confirm } = Modal

export class AdminListProject extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      selectedRowKeys: [],
      project: [],
      _visible: false,
      _type: null,
      _uid: null,
      filter: {
        search: null,
        type: 'name',
        orderBy: '',
      }
    }
  }

  componentWillMount() {
    const getLocal = localStorage.getItem('selectedProject')
    if(getLocal){
      localStorage.removeItem('selectedProject')
    }
  }

  componentDidMount() {
    const { actionGetData, match: { params }, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": '',
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        actionGetData(params.id, current.page, this.state.perPage, filter)
      })
    }else{
      actionGetData(params.id, 1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData, match: { params } } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(params.id, e.current, e.pageSize, this.state.filter)
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { history, actionGetData, match: { params } } = this.props
    return this.setState({ "current": 1, "filter": e  }, () => {
      history.push()
      window.scrollTo(0, 0);
      return actionGetData(params.id, this.state.current, this.state.perPage, this.state.filter);
    })
  }

  renderModal(_type, _uid){
    switch (_type) {
      case 'detailCredential':
        return <DetailCredential onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} onCancel={this.onCancel} />
      case 'detailUkuran':
        return <DetailSize onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} onCancel={this.onCancel} />
      case 'editSkill':
        return <DetailProjectSkill onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} onCancel={this.onCancel} />
      case 'detailProses':
        return <DetailProses onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} onCancel={this.onCancel} />
      default:
        return null
    }
  }

  renderTitle(_type){
    switch (_type) {
      case 'detailCredential':
        return <Text>Ubah Detail Informasi</Text>
      case 'detailUkuran':
        return <Text>Ubah Detail Ukuran</Text>
      case 'editSkill':
        return <Text>Ubah Detail Tukang</Text>
      case 'detailProses':
        return <Text>Detail Proses</Text>
      default:
        return null
    }
  }

  onCancel = () => {
    this.setState({
      _visible: false,
      _type: null,
      _uid: null
    })
  }

  onCloseModal = () => {
    const { actionGetData, match: { params } } = this.props
    const { current } = this.state
    actionGetData(params.id, current, this.state.perPage)
    this.setState({
      _visible: false,
      _type: null,
      _uid: null
    })
  }

  openModal(type, uid){
    this.setState({
      _visible: true,
      _type: type,
      _uid: uid
    })
  }

  deleteConfirm = (id) => {
    const { current } = this.state
    const { actionSetDelete, actionGetData, match: { params } } = this.props
    const payload = {
      "uid": id
    }
    confirm({
      title: 'Apa anda yakin ingin menghapus project ini?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Hapus',
      okType: 'danger',
      cancelText: 'Cancel',
      onOk() {
        actionSetDelete(payload, async (response) => {
          message.success(response.status.message)
          return actionGetData(params.id, current)
        }, (err) => {
          message.error(err.message)
        })
      },
    });
  }

  onPrint = () => {
    const { project } = this.state
    const { actionSetPrint, match: { params } } = this.props
    const payload = {
      'uid': params.id,
      'project': project
    }
    confirm({
      title: 'Apa anda ingin melakukan print loketing?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Ya',
      okType: 'primary',
      cancelText: 'Batal',
      onOk() {
        return actionSetPrint(payload, (response) => {
          const download = response.body
          window.open(download)
        }, () => message.error('Data Tidak Lengkap! Mohon Lengkapi Data Pemilihan Tukang'))
      },
    });
  }

  onSelectChange = (selectedRowKeys, value) => {
    this.setState({ project: selectedRowKeys }, () => {
      return localStorage.setItem('selectedProject', JSON.stringify(selectedRowKeys))
    });
  };

  render() {
    const { match: { params }, getData: { data, loading, pagination } } = this.props
    const { _visible, _type, _uid, selectedRowKeys, current, project, perPage } = this.state
    const defaultStatusFilter = [
      { value: 'name', name: 'Nama' },
      { value: 'noPot', name: 'No. Pot' },
      { value: 'size', name: 'Status Ukuran' },
      { value: 'skill', name: 'Status Kemampuan' },
      { value: 'scan', name: 'Status Scan' },
    ]
    const rowSelection = {
      project,
      onChange: this.onSelectChange,
      preserveSelectedRowKeys: true,
      selections: true
    }
    const hasSelected = project.length > 0;
    return (
      <React.Fragment>
        <PageHeader 
          className="site-page-header" 
          title="TABEL PROYEK" 
          ghost={true}
          onBack={() => this.props.history.push('/admin')}
          extra={[
            <Menu key="1" selectedKeys="2" mode="horizontal" style={{ background: 'none' }}>
              <Menu.Item key="1" icon={<DatabaseOutlined />}>
                <Link to={`/admin/project/planning/detail/${params.id}`}>
                  <Text>PERENCANAAN PROYEK</Text>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<TableOutlined />}>
                <Link to={`/admin/project/list/${params.id}`}>
                  <Text>TABEL PROYEK</Text>
                </Link>
              </Menu.Item>
            </Menu>,
          ]}
        />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterProject 
              fase1={null}
              fase2={{ key: 'type', value: defaultStatusFilter}}
              onSearch={this.onSearch}
            />
          </Col>
          <Col sm={24} md={24} style={{marginBottom: 30}}>
            <Button onClick={() => this.onPrint(selectedRowKeys)} size="large" type="dashed" icon={<PrinterOutlined />} disabled={!hasSelected} block>
              Print Tukang Untuk Loket
            </Button>
          </Col>
          <Col sm={24} md={24}>
            <Table 
              rowKey={(i) => i.uid}
              loading={loading} 
              dataSource={data} 
              columns={columns(current, perPage, (type, uid) => this.openModal(type, uid), this.deleteConfirm)} 
              pagination={pagination}
              onChange={this.pagination}
              scroll={{ x: 1800 }}
              rowSelection={rowSelection}
            />
          </Col>
        </Row>

        <Modal
          title={this.renderTitle(_type)}
          visible={_visible}
          onOk={this.onCloseModal}
          onCancel={this.onCancel}
          centered
          footer={false}
          width={1000}
          destroyOnClose={true}
        >
          {this.renderModal(_type, _uid)}
        </Modal>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.list
})

const mapDispatchToProps = {
  actionGetData     : listProject,
  actionSetPrint    : printLoketing,
  actionSetDelete   : deleteListProject,
  actionGetDetail   : detailProject,
  unmountGetDetail  : unmountDataProject
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminListProject)
