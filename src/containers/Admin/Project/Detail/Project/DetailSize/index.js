import React, { Component } from 'react'
import update from 'react-addons-update'
import { connect } from 'react-redux'
import { Loading } from '../../../../../../components'
import { Card, Col, InputNumber, Form, Row, Button, Divider, message } from 'antd'
import { detailProject, editProject, unmountDetailProject } from '../../../../../../redux/actions/project/projectAction'
 
export class DetailProjectSize extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _form: [],
       _loading: true,
       submitLoading: false
    }
  }
  
  componentDidMount() {
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid, 'size', (res) => {
      return this.setState({
        _form: res.body,
        _loading: false
      })
    })
  }

  onChange(value, indexType, keyValue){
    return this.setState({
      _form: update(this.state._form, {
        [indexType]: {
          value: {
            [keyValue]: {$set: value}
          }
        }
      })
    })
  }

  onFinish = () => {
    const { _form } = this.state; 
    const { actionSetEdit, uid, onCloseModal, unmountGetDetail } = this.props
    const payload = {
      size: _form,
      uid: uid,
      type: 'size'
    }
    this.setState({ submitLoading: true })
    actionSetEdit(payload, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        onCloseModal()
        unmountGetDetail()
        return message.success(response.status.message)
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }

  onCancel = () => {
    const { onCancel } = this.props
    return onCancel()
  }

  render() {
    const { _loading, _form } = this.state

    if(_loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <Row gutter={16}>
          <Col sm={24} md={24}>
            {
              _form.map((item, i) => (
                <Card title={item.typeSize} key={i} style={{ marginBottom: 30 }}>
                  {
                    Object.entries(item.value).map((item_, i_) => (
                      <Row key={i_} style={{marginBottom: 20}}>
                        <Col sm={24} md={24}>
                          <Form labelCol={{ span: 4 }} wrapperCol={{ span: 16 }}>
                            <Form.Item label={`${item_[0]} (cm)`}>
                              <InputNumber size="large" value={item_[1] ? item_[1] : 0} onChange={(e) => this.onChange(e, i, item_[0])} />
                            </Form.Item>
                          </Form>
                        </Col>
                      </Row> 
                    ))
                  }
                </Card>
              ))
            }
          </Col>
        </Row>

        <Divider />

        <Col style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Button  size="large" type="danger" onClick={() => this.onCancel()} style={{ marginRight: 10 }}>
            Batal
          </Button>
          <Button size="large" type="primary" onClick={this.onFinish} loading={this.state.submitLoading}>
            Simpan
          </Button>
        </Col>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailProject,
  actionSetEdit     : editProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProjectSize)
