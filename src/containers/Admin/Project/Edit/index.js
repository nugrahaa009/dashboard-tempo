import React, { Component } from 'react'
import moment from 'moment'
import update from 'react-addons-update'
import { connect } from 'react-redux'
import { Loading } from '../../../../components'
import { PageHeader, Form, Input, Switch, Card, Row, Col, Button, Select, Modal, DatePicker, Space, Upload, Typography, message } from 'antd'
import { MinusCircleOutlined, PlusOutlined, ExclamationCircleOutlined, PaperClipOutlined, UploadOutlined } from '@ant-design/icons'
import { detailPlanning, editPlanning, holdPlanning } from '../../../../redux/actions/planning/planningAction'
import { listDataMaster, unmountDataMaster } from '../../../../redux/actions/master/actionMaster'

const { confirm } = Modal
const { Text } = Typography

export class AdminEditProject extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      submitLoading: false,
      loadingSkill: true,
      fileList: null,
      fileName: null,
      skill: []
    }
  }
  
  componentDidMount() {
    const { actionGetMaster, actionGetDetail, match: { params } } = this.props
    const type = [ 'master_skills' ]

    actionGetMaster(type, () => {
      return this.setState({ loadingFilter: false })
    })
    actionGetDetail(params.id, (response) => {
      this.setState({
        skill: response.body.skill
      })
    })
  }

  onChange = (value) => {
    this.setState({
      skill: update(this.state.skill, {
        $push: [value]
      })
    })
  }

  removeState = (index) => {
    this.setState({
      skill: update(this.state.skill, {
        $splice: [[index, 1]]
      })
    })
  }

  modalConfirm = (value) => {
    const { actionSetHold, actionGetDetail, match: { params } } = this.props
    const payload = {
      uid: params.id,
      isHold: value
    }
    confirm({
      title: 'Apakah anda ingin merubah status proyek ini?',
      icon: <ExclamationCircleOutlined />,
      onOk() {
        actionSetHold(payload, () => {
          message.success('On hold project success')
          return actionGetDetail(params.id)
        }, (err) => {
          return message.error(err.message)
        })
      },
    })
  }

  handleUpload(){
    return {
      showUploadList: false,
      withCredentials: true,
      accept:".csv",
      beforeUpload: file => {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.setState({ 
            fileList: file, 
            fileName: file.name
          })
        }
        return false;
      }
    }
  }

  onFinish = (values) => {
    const { fileList } = this.state
    const { actionSetEdit, match: { params }, history } = this.props
    const newValue = values
    newValue.uid = params.id
    newValue.deadline = moment(values.deadline).format('YYYY-MM-DD')
    newValue.size = JSON.stringify(values.size)
    newValue.skill = JSON.stringify(values.skill)
    newValue.notes = values.notes !== undefined ? values.notes : null
    this.setState({ submitLoading: true })
    const payload = {
      file: fileList,
      ...newValue
    }
    return actionSetEdit(payload, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        message.success(response.status.message)
        return history.push(`/admin/project/planning/detail/${params.id}`)
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }
  
  render() {
    const { loadingFilter } = this.state
    const { getData: { data, loading }, getDataSkill, match: { params } } = this.props
    if(getDataSkill.loading || loading || loadingFilter){
      return <Loading />
    }
    const dataType = data.size.map(item => ({
      typeSize: item.typeSize,
      value: Object.entries(item.value).map(res => res[1])
    }))

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="EDIT PROJECT" onBack={() => this.props.history.push(`/admin/project/planning/detail/${params.id}`)} />
        <Form 
          onFinish={this.onFinish} 
          name="normal_register" 
          layout="vertical"
          initialValues={{
            size: dataType,
            skill: data.skill
          }}
        >
          <Card title="Informasi Planning Project" style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={24} md={12}>
                <Form.Item label="Nama Proyek" name="name" rules={[{ required: true, message: 'Nama Proyek Tidak Boleh Kosong!' }]} initialValue={data.name}>
                  <Input placeholder="Nama Proyek" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Nama Perusahaan" name="companyName" rules={[{ required: true, message: 'Nama Perusahaan Tidak Boleh Kosong!' }]} initialValue={data.companyName ? data.companyName : null}>
                  <Input placeholder="Nama Perusahaan" size="large" />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item 
                  label="Jumlah Pesanan" 
                  name="total"
                  rules={[
                    { required: true, message: 'Jumlah Pesanan Tidak Boleh Kosong!' },
                    { pattern: /^[0-9]*$/, message: 'Hanya Bisa Menggunakan Angka!' },
                  ]}
                  initialValue={data.total}
                >
                  <Input size="large" placeholder="Jumlah Pesanan" style={{ width: '100%' }} />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="DOD" name="deadline" rules={[{ required: true, message: 'DOD Tidak Boleh Kosong!' }]} initialValue={data.deadline ? moment(data.deadline, 'YYYY-MM-DD') : null}>
                  <DatePicker 
                    placeholder="Pilih Tanggal" 
                    style={{ width: '100%' }} 
                    size="large" 
                    disabledDate={(current) => {
                      return current && current < moment().add(-1, "days");
                    }}
                  />
                </Form.Item>
              </Col>
              <Col sm={24} md={12}>
                <Form.Item label="Unggah CSV" rules={[{ required: true, message: 'Unggah CSV Tidak Boleh Kosong!' }]}>
                  <Space>
                    <Upload {...this.handleUpload()}>
                      <Button type="primary" icon={<UploadOutlined />}>Unggah CSV</Button>
                    </Upload>
                    { this.state.fileName &&
                      <Text>
                        <PaperClipOutlined /> {this.state.fileName}
                      </Text>
                    }
                  </Space>
                </Form.Item>
              </Col>
              <Col sm={24} md={24}>
                <Form.Item label="Status" initialValue={data.isHold}>
                  Proyek Berlangsung <Switch onChange={this.modalConfirm} checked={data ? data.isHold : null} size="default" /> Proyek Ditunda
                </Form.Item>
              </Col>
              <Col sm={24} md={24}>
                <Form.Item label="Keterangan" name="notes" initialValue={data.notes}>
                  <Input.TextArea placeholder="Keterangan" size="large" />
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card title="Pilih Ukuran" style={{ marginBottom: 30 }}>
            <Form.List name="size">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(field => (
                    <Row gutter={16} key={field.fieldKey}>
                      <Col sm={24} md={8}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'typeSize']}
                          fieldKey={[field.fieldKey, 'typeSize']}
                          rules={[{ required: true, message: 'Jenis Ukuran Tidak Boleh Kosong!' }]}
                        >
                          <Input placeholder="Jenis Ukuran" size="large" disabled={data.isEditProject} />
                        </Form.Item>
                      </Col>
                      <Col sm={24} md={16}>
                        <Form.Item
                          {...field}
                          name={[field.name, 'value']}
                          fieldKey={[field.fieldKey, 'value']}
                          rules={[{ required: true, message: 'Proses Tidak Boleh Kosong!' }]}
                          extra="Note: Silahkan menggunakan (, atau enter) untuk membuat lebih dari 1"
                        >
                          <Select mode="tags" tokenSeparators={[',']} placeholder="Proses" size="large" dropdownStyle={{ display: 'none' }} disabled={data.isEditProject} />
                        </Form.Item>
                      </Col>
                    </Row>
                  ))}
                </>
              )}
            </Form.List>
          </Card>

          <Card title="Pilih Kemampuan" style={{ marginBottom: 30 }}>
            <Form.List name="skill">
              {(fields, { add, remove }) => (
                <>
                  {fields.map((field, index) => (
                    <Row gutter={16} key={field.fieldKey}>
                      <Col sm={24} md={23}>
                        <Form.Item 
                          {...field}
                          rules={[{ required: true, message: 'Pilih Kemampuan Tidak Boleh Kosong!' }]}
                        >
                          <Select
                            showSearch
                            style={{ width: '100%' }}
                            placeholder="Pilih Kemampuan"
                            size="large"
                            onChange={this.onChange}
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                            disabled={data.isEditProject}
                          >
                            {
                              getDataSkill.data && getDataSkill.data.master_skills.map((res, i) => {
                                const act = this.state.skill.includes(res.skillName || data.skill);
                                return (
                                  <React.Fragment>
                                    {
                                      (!act) && 
                                      <Select.Option key={i} value={res.skillName}>
                                        {res.skillName}
                                      </Select.Option>
                                    }
                                  </React.Fragment>
                                )
                              })
                            }
                          </Select>
                        </Form.Item>
                      </Col>
                      {
                        !data.isEditProject ?
                        <Col sm={1} md={1} style={{ paddingTop: 7 }}>
                          <MinusCircleOutlined onClick={() => {
                            remove(field.name);
                            return this.removeState(index)
                          }} />
                        </Col>
                        :
                        null
                      }
                    </Row>
                  ))}
                  <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />} disabled={data.isEditProject}>
                      Tambah Form
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </Card>

          <Row gutter={16}>
            <Col span={24} style={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Button className="register-form-button" size="large" type="danger" onClick={() => window.history.back()} style={{ marginRight: 10 }}>
                Batal
              </Button>
              <Button loading={this.state.submitLoading} size="large" type="primary" htmlType="submit" className="register-form-button">
                Simpan
              </Button>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  getDataSkill  : state.master,
  getData       : state.planning.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailPlanning,
  actionSetEdit     : editPlanning,
  actionSetHold     : holdPlanning,
  actionGetMaster   : listDataMaster,
  unmountDataMaster : unmountDataMaster
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminEditProject)
