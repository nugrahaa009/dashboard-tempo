import React, { Component } from 'react'
import { columns } from './columns'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { FilterTable } from '../../../components'
import { PageHeader, Row, Col, Table, Button, Modal, Card, message } from 'antd'
import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons'
import { listPlanning, unmountDataPlanning } from '../../../redux/actions/planning/planningAction'
import { deleteProject } from '../../../redux/actions/project/projectAction'
const qs = require('qs')
const { confirm } = Modal

export class Admin extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(current.page, this.state.perPage, filter)
      })
    }else{
      return actionGetData(1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(e.current, e.pageSize, this.state.filter);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  deleteConfirm = (id) => {
    const { current, perPage } = this.state
    const { actionSetDelete, actionGetData } = this.props
    const payload = {
      "uid": id
    }
    confirm({
      title: 'Apa anda yakin ingin menghapus project ini?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Hapus',
      okType: 'danger',
      cancelText: 'Cancel',
      onOk() {
        actionSetDelete(payload, (response) => {
          message.success(response.status.message)
          return actionGetData(current, perPage)
        }, (err) => {
          message.error(err.message)
        })
      },
    });
  }

  onSearch = (e) => {
    const { history, actionGetData } = this.props
    return this.setState({ "current": 1, "filter": e  }, () => {
      history.push()
      window.scrollTo(0, 0);
      return actionGetData(this.state.current, this.state.perPage, this.state.filter);
    })
  }
  
  render() {
    const { current, perPage } = this.state
    const { getData: { data, loading, pagination } } = this.props
    const defaultStatusFilter = [
      { value: 'name', name: 'Nama Order' },
    ]
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="PROJECT" />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterTable 
              fase1={null}
              fase2={{ key: 'type', value: defaultStatusFilter}}
              onSearch={this.onSearch}
            />
          </Col>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Link to="/admin/project/create">
              <Button size="large" type="dashed"  icon={<PlusOutlined />} block>
                Tambah Proyek
              </Button>
            </Link>
          </Col>
          <Card>
            <Col sm={24} md={24}>
              <Table 
                rowKey={(i) => i.uid}
                loading={loading} 
                dataSource={data} 
                columns={columns(current, perPage, this.deleteConfirm)} 
                pagination={pagination} 
                onChange={this.pagination} 
              />
            </Col>
          </Card>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData : state.planning.list
})

const mapDispatchToProps = {
  actionGetData   : listPlanning,
  actionSetDelete : deleteProject,
  unmountGetData  : unmountDataPlanning
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin)
