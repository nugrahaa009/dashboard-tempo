import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { Loading } from '../../../components'
import { number } from '../../../helper/pagination'
import { Card, Row, Col, Progress, Table, Typography} from 'antd'
import { dashboardData } from '../../../redux/actions/dashboard/dashboardAction'
import { listPlanning, unmountDataPlanning } from '../../../redux/actions/planning/planningAction'

const qs = require('qs')
const { Text } = Typography

export class Stakeholder extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetData, actionGetChart, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    actionGetChart(filter)
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(current.page, this.state.perPage, filter)
      })
    }else{
      return actionGetData(1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(e.current, e.pageSize, this.state.filter);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  render() {
    const { current, perPage } = this.state
    const { getData: { data, loading, pagination }, getDashboard } = this.props
    const dataChart = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
      datasets: [{
				label: 'Pengeluaran',
				backgroundColor: '#2711A2',
				data: getDashboard && getDashboard.projectPayment
			}]
    }
    if(loading){
      return <Loading />
    }
    const columns = [
      {
        title: 'No',
        ellipsis: true,
        align: 'left',
        width: 80,
        render: (data, text, index) => number(index, current, perPage)
      },
      {
        title: 'Nama Proyek',
        width: 200,
        render: (record) => (
          <Text>
            {record ? record.name : '-'} 
          </Text>
        )
      },
      {
        title: 'Progress',
        render: (record) => (
          <Progress
            strokeLinecap="square"
            strokeColor={{
              '0%': '#108ee9',
              '100%': '#87d068',
            }}
            percent={record.progress}
          />
        )
      },
    ]

    return (
      <React.Fragment>
        <Card title="Laporan Pembayaran" style={{ marginBottom: 30 }}>
          <Row gutter={16}>
            <Col sm={24} md={24}>
              <Bar data={dataChart} />
          </Col>
          </Row>
        </Card>
        <Card title="Laporan Proyek">
          <Row gutter={16}>
            <Col sm={24} md={24}>
              <Table 
                rowKey={(i) => i.uid}
                loading={loading} 
                dataSource={data} 
                columns={columns} 
                pagination={pagination} 
                onChange={this.pagination} 
              />
            </Col>
          </Row>
        </Card>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData       : state.planning.list,
  getDashboard  : state.dashboard.data

})

const mapDispatchToProps = {
  actionGetData   : listPlanning,
  actionGetChart  : dashboardData,
  unmountGetData  : unmountDataPlanning
}

export default connect(mapStateToProps, mapDispatchToProps)(Stakeholder)
