import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { number } from '../../../../helper/pagination'
import { Typography, Tag, Button, Space, Tooltip } from 'antd'
import { CheckCircleOutlined, SearchOutlined } from '@ant-design/icons'
import NumberFormat from 'react-number-format'
const { Text } = Typography

export const columns = (current, perPage) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 80,
      render: (data, text, index) => number(index, current, perPage)
    },
    {
      title: 'No.Invoice',
      align: 'center',
      render: (record) => (
        <Text>
          {record.invoiceNumber ? record.invoiceNumber : 0} 
        </Text>
      )
    },
    {
      title: 'Nama Proyek',
      render: (record) => (
        <Text>
          {record ? record.planProject.name : '-'} 
        </Text>
      )
    },
    {
      title: 'Tanggal Pembayaran',
      align: 'center',
      render: (record) => (
        <Text>
          {record.updated_at ? moment(record.updated_at).format('LL') : '-'} 
        </Text>
      )
    },
    {
      title: 'Jumlah Pembayaran',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.totalPrice ? record.totalPrice.toLocaleString() : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          prefix={'Rp '} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
    {
      title: 'Status Pemabayaran',
      align: 'center',
      render: () => (
        <React.Fragment>
          <Tag icon={<CheckCircleOutlined />} color="success">
            Sudah Dibayar
          </Tag>
        </React.Fragment>
      )
    },
    {
      title: '',
      align: 'right',
      render: (record) => (
        <Space>
          <Link to={`/stakeholder/history/detail/${record.invoiceNumber}`}>
            <Tooltip placement="bottomRight" title="Lihat Detail Pembayaran">
              <Button type="primary" shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
          </Link>
        </Space>
      )
    },
  ]
}