import React, { Component } from 'react'
import { Loading } from '../../../../../components'
import { columns } from './columns'
import { connect } from 'react-redux'
import { PageHeader, Row, Col, Table, Typography } from 'antd'
import { detailHistory, unmountDetailHistory } from '../../../../../redux/actions/payment/paymentAction'
import NumberFormat from 'react-number-format'

const { Title } = Typography

export class StakeholderDetailHistory extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       current: 1
    }
  }

  componentDidMount() {
    const { actionGetDetail, match: { params } } = this.props
    return actionGetDetail(params.id)
  }

  render() {
    const { current } = this.state
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="DETAIL RIWAYAT" onBack={() => this.props.history.push('/stakeholder/history')} />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={24} md={24}>
                <Table 
                  rowKey={this.state.current}
                  dataSource={data} 
                  columns={columns(current)} 
                  loading={loading} 
                  pagination={false}
                  summary={pageData => {
                    let _totalPrice = 0;
                    pageData.forEach(({ totalPrice }) => {
                      _totalPrice += totalPrice;
                    });
            
                    return (
                      <>
                        <Table.Summary.Row style={{ textAlign: 'center' }}>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <Title level={3}>Jumlah</Title>
                          </Table.Summary.Cell>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <NumberFormat 
                              value={_totalPrice ? _totalPrice : 0} 
                              displayType={'text'} 
                              thousandSeparator={true} 
                              prefix={'Rp '} 
                              renderText={value => <Title level={3}>{value}</Title>} 
                            />
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                      </>
                    );
                  }}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.invoice.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailHistory,
  unmountGetDetail  : unmountDetailHistory
}

export default connect(mapStateToProps, mapDispatchToProps)(StakeholderDetailHistory)
