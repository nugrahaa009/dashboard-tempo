import React from 'react'
import { Typography } from 'antd'
import { number } from '../../../../../helper/pagination'
import NumberFormat from 'react-number-format';
const { Text } = Typography

export const columns = (current) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 80,
      render: (data, text, index) => number(index, current, 10)
    },
    {
      title: 'Nama Proyek',
      sorter: (a, b) => a.planProject.name.length - b.planProject.name.length,
      render: (record) => (
        <Text>
          {record.planProject.name ? record.planProject.name : '-'} 
        </Text>
      )
    },
    {
      title: 'Pengerjaan',
      render: (record) => (
        <Text>
          {record.skill ? record.skill : '-'} 
        </Text>
      )
    },
    {
      title: 'Jumlah Pengerjaan',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.totalSkill ? record.totalSkill : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
    {
      title: 'Harga Satuan',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.price ? record.price : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          prefix={'Rp '} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
    {
      title: 'Jumlah',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.totalPrice ? record.totalPrice : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          prefix={'Rp '} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
  ]
}