import React, { Component } from 'react'
import { columns } from './columns'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { FilterExportExcel } from '../../../../components'
import { DollarOutlined, HistoryOutlined } from '@ant-design/icons'
import { PageHeader, Menu, Typography, Row, Col, Table } from 'antd'
import { listPayment, unmountDataPayment } from '../../../../redux/actions/payment/paymentAction'

const { Text } = Typography

const qs = require('qs')

export class StakeholderPayment extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(current.page, this.state.perPage, filter)
      })
    }else{
      return actionGetData(1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(e.current, e.pageSize, this.state.filter);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { history, actionGetData } = this.props
    const _filter = {
      type: 'name',
      search: e.name !== undefined ? e.name : null,
      orderBy: 'desc',
    }
    return this.setState({ "current": 1, "filter": _filter  }, () => {
      history.push()
      window.scrollTo(0, 0);
      return actionGetData(this.state.curent, this.state.perPage, this.state.filter);
    })
  }

  render() {
    const { current, perPage } = this.state
    const { getData: { data, loading, pagination } } = this.props
    return (
      <React.Fragment>
        <PageHeader 
          className="site-page-header" 
          title="PEMBAYARAN" 
          ghost={true}
          extra={[
            <Menu key="1" selectedKeys="2" mode="horizontal" style={{ background: 'none' }}>
              <Menu.Item key="1" icon={<HistoryOutlined />}>
                <Link to={`/stakeholder/history`}>
                  <Text>RIWAYAT</Text>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<DollarOutlined />}>
                <Link to={`/stakeholder/payment`}>
                  <Text>PEMBAYARAN</Text>
                </Link>
              </Menu.Item>
            </Menu>,
          ]}
        />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterExportExcel onSearch={this.onSearch} />
          </Col>
          <Col sm={24} md={24}>
            <Table 
              rowKey={(i) => i.planProjectId}
              loading={loading} 
              dataSource={data} 
              columns={columns(current, perPage)} 
              pagination={pagination} 
              onChange={this.pagination} 
            />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData : state.payment.list
})

const mapDispatchToProps = {
  actionGetData   : listPayment,
  unmountGetData  : unmountDataPayment
}

export default connect(mapStateToProps, mapDispatchToProps)(StakeholderPayment)
