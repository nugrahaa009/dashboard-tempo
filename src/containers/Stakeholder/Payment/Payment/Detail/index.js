import React, { Component } from 'react'
import { Loading } from '../../../../../components'
import { columns } from './columns'
import { connect } from 'react-redux'
import { PageHeader, Row, Col, Typography, Table } from 'antd'
import { detailPayment, unmountDetailPayment } from '../../../../../redux/actions/payment/paymentAction'
import NumberFormat from 'react-number-format';
const { Text, Title } = Typography

export class DetailPayment extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       current: 1
    }
  }

  componentDidMount() {
    const { actionGetDetail, match: { params } } = this.props
    return actionGetDetail(params.id)
  }

  render() {
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="DETAIL PEMBAYARAN" onBack={() => this.props.history.push('/stakeholder/payment')} />
        <Row justify="space-around" align="middle" style={{marginBottom: 30}}>
          <Col flex={4}>
            <Text strong>Status: </Text>
            <Text type="danger">Belum Bayar</Text>
          </Col>
        </Row>
        <Row gutter={16}>

          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Row gutter={16}>
              <Col sm={24} md={24}>
                <Table 
                  rowKey={this.state.current}
                  dataSource={data} 
                  columns={columns(this.state.current)} 
                  loading={loading} 
                  pagination={false}
                  summary={pageData => {
                    let _totalPrice = 0;

                    pageData.forEach(({ totalPrice }) => {
                      _totalPrice += totalPrice;
                    });
            
                    return (
                      <>
                        <Table.Summary.Row style={{ textAlign: 'center' }}>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <Title level={3}>Jumlah</Title>
                          </Table.Summary.Cell>
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell />
                          <Table.Summary.Cell>
                            <NumberFormat 
                              value={_totalPrice ? _totalPrice : 0} 
                              displayType={'text'} 
                              thousandSeparator={true} 
                              prefix={'Rp '} 
                              renderText={value => <Title level={3}>{value}</Title>} 
                            />
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                      </>
                    );
                  }}
                />
              </Col>
            </Row>

          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.payment.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailPayment,
  unmountGetDetail  : unmountDetailPayment
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPayment)
