import React from 'react'
import moment from 'moment'
import NumberFormat from 'react-number-format'
import { number } from '../../../../helper/pagination'
import { Typography } from 'antd'
const { Text } = Typography

export const columns = (current, perPage) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 80,
      render: (data, text, index) => number(index, current, perPage)
    },
    {
      title: 'Nama Proyek',
      sorter: (a, b) => a.project.planProject.name.length - b.project.planProject.name.length,
      render: (record) => (
        <Text>
          {record.project.planProject.name ? record.project.planProject.name : '-'} 
        </Text>
      )
    },
    {
      title: 'Pengerjaan',
      render: (record) => (
        <Text>
          {record.skill ? record.skill : '-'} 
        </Text>
      )
    },
    {
      title: 'Tanggal Pembayaran',
      align: 'center',
      render: (record) => (
        <Text>
          {record.datePay ? moment(record.datePay).format('LL') : '-'} 
        </Text>
      )
    },
    {
      title: 'Jumlah Pengerjaan',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.totalSkill ? record.totalSkill : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
    {
      title: 'Harga Satuan',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.price ? record.price : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          prefix={'Rp '} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
    {
      title: 'Jumlah',
      align: 'center',
      render: (record) => (
        <NumberFormat 
          value={record.total ? record.total : 0} 
          displayType={'text'} 
          thousandSeparator={true} 
          prefix={'Rp '} 
          renderText={value => <Text>{value}</Text>} 
        />
      )
    },
  ]
}