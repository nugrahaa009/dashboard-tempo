import React from 'react'
import { Link } from 'react-router-dom'
import { number } from '../../../helper/pagination'
import { Button, Typography, Dropdown, Menu } from 'antd'
import { MoreOutlined, HistoryOutlined, FileSearchOutlined } from '@ant-design/icons'
const { Text } = Typography
const menu = (onDetail, type, uid) => (
  <Menu>
    <Menu.Item icon={<FileSearchOutlined />} onClick={() => onDetail(type, uid)}>Detail Pengguna</Menu.Item>
    {
      type === 3 ?
      <Menu.Item icon={<HistoryOutlined />}>
        <Link to={`/stakeholder/user/billing-payment/${uid}`}>
          Riwayat Penagihan
        </Link>
      </Menu.Item>
      :
      null
    }
  </Menu>
)

export const columns = (current, perPage, onDetail) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 80,
      render: (data, text, index) => number(index, current, perPage)
    },
    {
      title: 'Nama',
      render: (record) => (
        <Text>
          {record.userContact ? record.userContact.fullName : '-'} 
        </Text>
      )
    },
    {
      title: 'Nomor Telepon',
      render: (record) => (
        <Text>
          {record ? record.phoneNumber : '-'} 
        </Text>
      )
    },
    {
      title: 'Jenis Kelamin',
      align: 'center',
      render: (record) => (
        <Text>
          {record.userContact ? record.userContact.gender === 'Male' ? 'Laki-Laki' : 'Perempuan' : '-'} 
        </Text>
      )
    },
    {
      title: 'Peran',
      align: 'center',
      render: (record) => (
        <Text>
          {record ? record.userType === 2 ? 'Admin' : null : '-'} 
          {record ? record.userType === 3 ? 'Tukang' : null : '-'} 
          {record ? record.userType === 4 ? 'Stakeholder' : null : '-'} 
          {record ? record.userType === 5 ? 'Quality Control' : null : '-'} 
          {record ? record.userType === 6 ? 'Fitting' : null : '-'} 
        </Text>
      )
    },
    {
      title: 'Status Pekerja',
      align: 'center',
      render: (record) => (
        <Text>
          {
            record.userType === 3 ?
            record.isStaff === 1 ?
            'Staff'
            :
            'Non Staff'
            :
            '-'
          }
        </Text>
      )
    },
    {
      title: '',
      align: 'right',
      render: (record) => (
        <React.Fragment>
          <Dropdown overlay={menu(onDetail, record.userType, record.uid)}>
            <Button type="primary" icon={<MoreOutlined />}/>
          </Dropdown>
        </React.Fragment>
      )
    },
  ]
}