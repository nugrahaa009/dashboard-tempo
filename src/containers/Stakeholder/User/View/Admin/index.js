import React, { Component } from 'react'
import { Loading } from '../../../../../components'
import { connect } from 'react-redux'
import { PageHeader, Typography, Row, Col, Card } from 'antd'
import { detailUser, unmountDetailUser } from '../../../../../redux/actions/user/userAction'
const { Title, Text } = Typography

export class StakeholderDetailAdmin extends Component {

  componentDidMount() {
    const { actionGetDetail, match: { params } } = this.props
    return actionGetDetail(params.id)
  }

  render() {
    const { getData: { data, loading } } = this.props
    if(loading){ return <Loading /> }
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="DETAIL ADMIN" onBack={() => window.history.back()} />
        <Card title="Informasi Admin" style={{ marginBottom: 30 }}>
          <Row gutter={16}>
            <Col sm={24} md={12} style={{ marginBottom: 30 }}>
              <Title level={5}>NIP:</Title>
              <Text>{data.nip ? data.nip : 'N/A'}</Text>
            </Col>
            <Col sm={24} md={12} style={{ marginBottom: 30 }}>
              <Title level={5}>Nama:</Title>
              <Text>{data.userContact.fullName ? data.userContact.fullName : 'N/A'}</Text>
            </Col>
            <Col sm={24} md={12} style={{ marginBottom: 30 }}>
              <Title level={5}>Jenis Kelamin:</Title>
              <Text>{data.userContact.gender ? data.userContact.gender === 'Male' ? 'Laki-Laki' : 'Perempuan' : 'N/A'}</Text>
            </Col>
            <Col sm={24} md={12} style={{ marginBottom: 30 }}>
              <Title level={5}>Nomor Telepon:</Title>
              <Text>{data.phoneNumber ? data.phoneNumber : 'N/A'}</Text>
            </Col>
            <Col sm={24} md={24} style={{ marginBottom: 30 }}>
              <Title level={5}>Alamat:</Title>
              <Text>{data.userContact.address ? data.userContact.address : 'N/A'}</Text>
            </Col>
          </Row>
        </Card>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData : state.user.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailUser,
  unmountGetDetail  : unmountDetailUser
}

export default connect(mapStateToProps, mapDispatchToProps)(StakeholderDetailAdmin)
