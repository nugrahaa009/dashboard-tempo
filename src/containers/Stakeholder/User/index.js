import React, { Component } from 'react'
import { connect } from 'react-redux'
import { columns } from './columns'
import { FilterTable } from '../../../components'
import { Col, PageHeader, Row, Table } from 'antd'
import { listUser, unmountDataUser } from '../../../redux/actions/user/userAction'
const qs = require('qs')

export class StakeholderUser extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      userType: 0,
      _visible: false,
      _type: null,
      _uid: null,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true });
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(current.page, this.state.userType, this.state.perPage, filter)
      })
    }else{
      return actionGetData(1, this.state.userType, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(e.current, this.state.userType, e.pageSize, this.state.filter);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { actionGetData } = this.props
    return this.setState({ 
      'current': 1, 
      'userType': e.userType,  
      "filter": {
        'search': e.search,
        'orderBy': e.orderBy,
        'type': e.type
      }, 
    }, () => {
      return actionGetData(this.state.current, this.state.userType, this.state.perPage, this.state.filter);
    })
  }

  onDetail = (type, uid) => {
    const { history } = this.props
    switch(type){
      case 2:
        return history.push(`/stakeholder/user/detail/admin/${uid}`)
      case 3:
        return history.push(`/stakeholder/user/detail/tukang/${uid}`)
      case 4:
        return history.push(`/stakeholder/user/detail/stakeholder/${uid}`)
      case 5:
        return history.push(`/stakeholder/user/detail/quality-control/${uid}`)
      case 6:
        return history.push(`/stakeholder/user/detail/fitting/${uid}`)
      default:
        return null
    }
  }

  render() {
    const { getData: { data, loading, pagination } } = this.props
    const { current, perPage } = this.state
    const defaultStatusFilter = [
      { value: 'fullName', name: 'Nama Lengkap' },
    ]
    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="PENGGUNA" />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterTable 
              fase1={null}
              fase2={{ key: 'type', value: defaultStatusFilter}}
              fase3
              onSearch={this.onSearch}
            />
          </Col>
          <Col sm={24} md={24}>
            <Table 
              rowKey={(i) => i.uid}
              loading={loading} 
              dataSource={data} 
              columns={columns(current, perPage, this.onDetail)} 
              pagination={pagination} 
              onChange={this.pagination} 
            />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData: state.user.list
})

const mapDispatchToProps = {
  actionGetData   : listUser,
  unmountGetData  : unmountDataUser
}

export default connect(mapStateToProps, mapDispatchToProps)(StakeholderUser)
