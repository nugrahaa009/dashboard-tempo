import React from 'react'
import { number } from '../../../../../helper/pagination'
import { Typography, Tag, Menu, Dropdown, Button } from 'antd'
import { MoreOutlined, FileSearchOutlined, FormOutlined, CheckCircleOutlined, ExclamationCircleOutlined } from '@ant-design/icons'

const { Text } = Typography

const menu = (openModal, id) => (
  <Menu>
    <Menu.Item icon={<FileSearchOutlined />} onClick={() => openModal('detailCredential', id)}>Detail Informasi</Menu.Item>
    <Menu.Item icon={<FileSearchOutlined />} onClick={() => openModal('detailUkuran', id)}>Detail Ukuran</Menu.Item>
    <Menu.Item icon={<FileSearchOutlined />} onClick={() => openModal('editSkill', id)}>Detail Tukang</Menu.Item>
    <Menu.Item icon={<FileSearchOutlined />} onClick={() => openModal('detailProses', id)}>Detail Progress</Menu.Item>
    <Menu.Item icon={<FormOutlined />} onClick={() => openModal('notes', id)}>Catatan</Menu.Item>
  </Menu>
)

export const columns = (current, perPage, openModal) => {
  return [
    {
      title: 'No',
      ellipsis: true,
      align: 'left',
      width: 60,
      render: (data, text, index) => number(index, current, perPage)
    },
    {
      title: 'No.Pot',
      align: 'center',
      width: 80,
      render: (record) => (
        <Text>
          {record !== null ? record.noPot : '-'}
        </Text>
      )
    },
    {
      title: 'No.Seri',
      align: 'center',
      width: 80,
      render: (record) => (
        <Text>
          {record !== null ? record.noSeri : '-'}
        </Text>
      )
    },
    {
      title: 'Isi',
      align: 'center',
      width: 80,
      render: (record) => (
        <Text>
          {record !== null ? record.isi : '-'}
        </Text>
      )
    },
    {
      title: 'Nama',
      width: 220,
      render: (record) => (
        <Text>
          {record !== null ? record.name : '-'}
        </Text>
      )
    },
    {
      title: 'Status Ukuran',
      align: 'center',
      width: 120,
      render: (record) => (
        <React.Fragment>
          {
            record.statusSize ?
            <Tag icon={<ExclamationCircleOutlined />} color="warning">
              Belum Lengkap
            </Tag>
            :
            <Tag icon={<CheckCircleOutlined />} color="success">
              Sudah Lengkap
            </Tag>
          }
        </React.Fragment>
      )
    },
    {
      title: 'Jumlah Ukuran',
      width: 120,
      align: 'center',
      render: (record) => (
        <Text>
          {record !== null ? record.totalSize : '-'}
        </Text>
      )
    },
    {
      title: 'Status Kemampuan',
      align: 'center',
      width: 120,
      render: (record) => (
        <React.Fragment>
          {
            record.statusSkill ?
            <Tag icon={<ExclamationCircleOutlined />} color="warning">
              Belum Lengkap
            </Tag>
            :
            <Tag icon={<CheckCircleOutlined />} color="success">
              Sudah Lengkap
            </Tag>
          }
        </React.Fragment>
      )
    },
    {
      title: 'Jumlah Kemampuan',
      align: 'center',
      width: 120,
      render: (record) => (
        <Text>
          {record !== null ? record.totalSkill : '-'}
        </Text>
      )
    },
    {
      title: 'Proses Scan',
      align: 'center',
      width: 120,
      render: (record) => (
        <React.Fragment>
          {
            record ? record.currentScan < record.totalSkill ?
            <Text type="danger">
              {record.currentScan}/{record.totalSkill}
            </Text>
            :
            <Text type="success">
              {record.currentScan}/{record.totalSkill}
            </Text>
            :
            '-'
          }
        </React.Fragment>
      )
    },
    {
      title: 'Status Scan',
      align: 'center',
      width: 120,
      render: (record) => (
        <React.Fragment>
          {
            record.isScan ?
            <Tag icon={<CheckCircleOutlined />} color="success">
              Sudah Lengkap
            </Tag>
            :
            <Tag icon={<ExclamationCircleOutlined />} color="warning">
              Belum Lengkap
            </Tag>
          }
        </React.Fragment>
      )
    },
    {
      title: '#',
      align: 'center',
      width: 60,
      fixed: 'right',
      render: (record) => (
        <React.Fragment>
          <Dropdown overlay={menu(openModal, record.uid)}>
            <Button type="primary" icon={<MoreOutlined />}/>
          </Dropdown>
        </React.Fragment>
      )
    },
  ]
}