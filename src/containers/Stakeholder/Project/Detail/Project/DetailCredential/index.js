import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../../../components'
import { Col, Typography, Row, Button, Divider } from 'antd'
import { detailProject, editProject, listProject, unmountDetailProject } from '../../../../../../redux/actions/project/projectAction'

const { Title, Text } = Typography

export class DetailProjectCredential extends Component {

  componentDidMount() {
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid)
  }

  onCancel = () => {
    const { onCloseModal } = this.props
    return onCloseModal()
  }
  
  render() {
    const { getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <Row gutter={16}>
          <Col sm={24} md={12} style={{ marginBottom: 30 }}>
            <Title level={5}>NO POT:</Title>
            <Text>{data.description["NO POT"]}</Text>
          </Col>
          <Col sm={24} md={12} style={{ marginBottom: 30 }}>
            <Title level={5}>NO SERI:</Title>
            <Text>{data.description["NO SERI"]}</Text>
          </Col>
          <Col sm={24} md={12} style={{ marginBottom: 30 }}>
            <Title level={5}>NAMA:</Title>
            <Text>{data.description["NAMA"]}</Text>
          </Col>
          <Col sm={24} md={12} style={{ marginBottom: 30 }}>
            <Title level={5}>PANGKAT:</Title>
            <Text>{data.description["PANGKAT"]}</Text>
          </Col>
          <Col sm={24} md={12} style={{ marginBottom: 30 }}>
            <Title level={5}>NIP:</Title>
            <Text>{data.description["NIP"]}</Text>
          </Col>
          <Col sm={24} md={12} style={{ marginBottom: 30 }}>
            <Title level={5}>KESATUAN:</Title>
            <Text>{data.description["KESATUAN"]}</Text>
          </Col>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <Title level={5}>KETERANGAN:</Title>
            <Text>{data.description["KET."]}</Text>
          </Col>
          <Divider />
          <Col sm={24} md={24} style={{ marginBottom: 15, display: 'flex', justifyContent: 'flex-end' }}>
            <Button size="large" type="danger" onClick={() => this.onCancel()}>
              Kembali
            </Button>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionSetEdit     : editProject,
  actionGetData     : listProject,
  actionGetDetail   : detailProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProjectCredential)
