import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../../../components'
import { Row, Col, Button, Typography, Space, Divider } from 'antd'
import { detailProject, unmountDetailProject, editProject } from '../../../../../../redux/actions/project/projectAction'
const { Text } = Typography

export class DetailProjectSkill extends Component {

  componentDidMount() {
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid, 'skill');
  }

  onCancel = () => {
    const { onCloseModal } = this.props
    return onCloseModal()
  }

  render() {
    const { getData: { loading, data } } = this.props

    if(loading){
      return <Loading />
    }
    
    return (
      <React.Fragment>
        {
          data.map((item, i) => (
            <Row key={i} style={{marginBottom: 20}}>
              <Col sm={24} md={24}>
                {
                  Object.entries(item).map((item_, i) => (
                    <Row gutter={16} key={i}>
                      <Col sm={24} md={12} style={{ display: 'flex' }}>
                        <Space>
                          <Text strong>
                            {item_[0] ? `${item_[0]}:` : 'N/A'}
                          </Text>
                          {
                            item_[1].value ?
                            <Text>
                              <Text>{item_[1].value.name}</Text> <Text type="secondary">({item_[1].value.staff})</Text>
                            </Text>
                            :
                            <Text type="danger">Belum Diisi</Text>
                          }
                        </Space>
                      </Col>
                    </Row>
                  ))
                }
              </Col>
            </Row> 
          ))
        }
        <Divider />
        <Col sm={24} md={24} style={{ marginBottom: 15, display: 'flex', justifyContent: 'flex-end' }}>
          <Button size="large" type="danger" onClick={() => this.onCancel()}>
            Kembali
          </Button>
        </Col>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionSetEdit     : editProject,
  actionGetDetail   : detailProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProjectSkill)
