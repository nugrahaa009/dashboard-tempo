import React, { Component } from 'react'
import DetailProjectSkill from './DetailSkill'
import DetailCredential from './DetailCredential'
import DetailSize from './DetailSize'
import DetailProses from './DetailProses'
import NotesProject from './Notes'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { columns } from './columns'
import { FilterProject } from '../../../../../components'
import { PageHeader, Menu, Typography, Row, Col, Table, Modal } from 'antd'
import { DatabaseOutlined, TableOutlined } from '@ant-design/icons'
import { detailProject, listProject, unmountDataProject } from '../../../../../redux/actions/project/projectAction'
const qs = require('qs')
const { Text } = Typography

export class StakeholderListProject extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 0,
      _visible: false,
      _type: null,
      _uid: null,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc',
      }
    }
  }

  componentDidMount() {
    const { actionGetData, match: { params }, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(params.id, current.page, this.state.perPage, filter)
      })
    }else{
      return actionGetData(params.id, 1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData, match: { params } } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(params.id, e.current, e.pageSize, this.state.filter)
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { history, actionGetData, match: { params } } = this.props
    return this.setState({ "current": 1, "filter": e  }, () => {
      history.push()
      window.scrollTo(0, 0);
      return actionGetData(params.id, this.state.current, this.state.perPage, this.state.filter);
    })
  }

  renderModal(_type, _uid){
    switch (_type) {
      case 'detailCredential':
        return <DetailCredential onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} />
      case 'detailUkuran':
        return <DetailSize onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} />
      case 'editSkill':
        return <DetailProjectSkill onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} />
      case 'detailProses':
        return <DetailProses onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} />
      case 'notes':
        return <NotesProject onCloseModal={this.onCloseModal} uid={_uid} current={this.state.current} />
      default:
        return null
    }
  }

  renderTitle(_type){
    switch (_type) {
      case 'detailCredential':
        return <Text>Detail Informasi</Text>
      case 'detailUkuran':
        return <Text>Detail Ukuran</Text>
      case 'editSkill':
        return <Text>Detail Tukang</Text>
      case 'detailProses':
        return <Text>Detail Proses</Text>
      case 'notes':
        return <Text>Keterangan</Text>
      default:
        return null
    }
  }

  onCloseModal = () => {
    const { actionGetData, match: { params } } = this.props
    actionGetData(params.id, this.state.current, this.state.perPage)
    this.setState({
      _visible: false,
      _type: null,
      _uid: null
    })
  }

  openModal(type, uid){
    this.setState({
      _visible: true,
      _type: type,
      _uid: uid
    })
  }

  render() {
    const { match: { params }, getData: { data, loading, pagination } } = this.props
    const { _visible, _type, _uid, perPage, current } = this.state
    const defaultStatusFilter = [
      { value: 'name', name: 'Nama' },
    ]
    return (
      <React.Fragment>
        <PageHeader 
          className="site-page-header" 
          title="TABEL PROYEK" 
          ghost={true}
          onBack={() => this.props.history.push('/stakeholder/project')}
          extra={[
            <Menu key="1" selectedKeys="2" mode="horizontal" style={{ background: 'none' }}>
              <Menu.Item key="1" icon={<DatabaseOutlined />}>
                <Link to={`/stakeholder/project/planning/detail/${params.id}`}>
                <Text>PERENCANAAN PROYEK</Text>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<TableOutlined />}>
                <Link to={`/stakeholder/project/list/${params.id}`}>
                  <Text>TABEL PROYEK</Text>
                </Link>
              </Menu.Item>
            </Menu>,
          ]}
        />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterProject 
              fase1={null}
              fase2={{ key: 'type', value: defaultStatusFilter}}
              onSearch={this.onSearch}
            />
          </Col>
          <Col sm={24} md={24}>
          <Table 
              rowKey={(i) => i.uid}
              loading={loading} 
              dataSource={data} 
              columns={columns(current, perPage, (type, uid) => this.openModal(type, uid), this.deleteConfirm)} 
              pagination={pagination}
              onChange={this.pagination}
              scroll={{ x: 1800 }}
              scrollToFirstRowOnChange={false}
            />
          </Col>
        </Row>

        <Modal
          title={this.renderTitle(_type)}
          visible={_visible}
          onOk={this.onCloseModal}
          onCancel={this.onCloseModal}
          centered
          footer={false}
          width={1000}
          destroyOnClose={true}
        >
          {this.renderModal(_type, _uid)}
        </Modal>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.list
})

const mapDispatchToProps = {
  actionGetData   : listProject,
  actionGetDetail : detailProject,
  unmountGetData  : unmountDataProject
}

export default connect(mapStateToProps, mapDispatchToProps)(StakeholderListProject)
