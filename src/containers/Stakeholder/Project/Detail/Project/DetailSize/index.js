import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading } from '../../../../../../components'
import { Card, Col, Row, Button, Typography, Space, Divider } from 'antd'
import { detailProject, editProject, unmountDetailProject } from '../../../../../../redux/actions/project/projectAction'

const { Text } = Typography
 
export class DetailProjectSize extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _form: [],
       _loading: true,
       submitLoading: false
    }
  }
  
  componentDidMount() {
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid, 'size', (res) => {
      return this.setState({
        _form: res.body,
        _loading: false
      })
    })
  }

  onCancel = () => {
    const { onCloseModal } = this.props
    return onCloseModal()
  }

  render() {
    const { _loading, _form } = this.state

    if(_loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <Row gutter={16}>
          <Col sm={24} md={24}>
            {
              _form.map((item, i) => (
                <Card title={item.typeSize} key={i} style={{ marginBottom: 30 }}>
                  {
                    Object.entries(item.value).map((item_, i_) => (
                      <Row key={i_} style={{marginBottom: 20}}>
                        <Col sm={24} md={12} style={{ display: 'flex' }}>
                          <Space>
                            <Text strong>{`${item_[0]}:`}</Text>
                            <Text>{item_[1] ? `${item_[1]} cm` : 0}</Text>
                          </Space>
                        </Col>
                      </Row> 
                    ))
                  }
                </Card>
              ))
            }
          </Col>
          <Divider />
          <Col sm={24} md={24} style={{ marginBottom: 15, display: 'flex', justifyContent: 'flex-end' }}>
            <Button size="large" type="danger" onClick={() => this.onCancel()}>
              Kembali
            </Button>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailProject,
  actionSetEdit     : editProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProjectSize)
