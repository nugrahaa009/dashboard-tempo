import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Form, Input, Row, Button, Divider, message } from 'antd'
import { detailProject, notesProject, unmountDetailProject } from '../../../../../../redux/actions/project/projectAction'

export class NotesProject extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      submitLoading: false
    }
  }

  componentDidMount() {
    const { actionGetDetail, uid } = this.props
    return actionGetDetail(uid)
  }
  
  onFinish = (values) => {
    const { getData: { data }, actionSetCreate, onCloseModal } = this.props
    const params = {
      "planProjectId": data.projectId,
      "projectId": data.uid,
      ...values
    }
    this.setState({ submitLoading: true })
    return actionSetCreate(params, async (response) => {
      return this.setState({ submitLoading: false }, () => {
        onCloseModal();
        return message.success(response.status.message);
      })
    }, (err) => {
      return this.setState({ submitLoading: false }, () => message.error(err.message))
    })
  }

  onCancel = () => {
    const { onCloseModal } = this.props
    return onCloseModal()
  }
  
  render() {

    return (
      <React.Fragment>
        <Form onFinish={this.onFinish} name="normal_register" layout="vertical">
          <Row gutter={16}>
            <Col sm={24} md={24}>
              <Form.Item name="notes">
                <Input.TextArea placeholder="Keterangan" rows={4} />
              </Form.Item>
            </Col>
          </Row>

          <Divider />

          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button size="large" type="danger" onClick={() => this.onCancel()} style={{ marginRight: 10 }}>
              Batal
            </Button>
            <Button size="large" type="primary" htmlType="submit" loading={this.state.submitLoading}>
              Simpan
            </Button>
          </div>

        </Form>
      </React.Fragment>
    )
  }
  
}

const mapStateToProps = (state) => ({
  getData: state.project.detail
})

const mapDispatchToProps = {
  actionSetCreate   : notesProject,
  actionGetDetail   : detailProject,
  unmountGetDetail  : unmountDetailProject
}

export default connect(mapStateToProps, mapDispatchToProps)(NotesProject)
