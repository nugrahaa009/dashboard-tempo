import React, { Component } from 'react'
import moment from 'moment'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Loading } from '../../../../../components'
import { PageHeader, Menu, Typography, Row, Col, Collapse, Empty, Divider, List, Tag, Comment, Tooltip, Badge } from 'antd'
import { DatabaseOutlined, TableOutlined } from '@ant-design/icons'
import { detailPlanning, unmountDetailPlanning } from '../../../../../redux/actions/planning/planningAction'
const { Panel } = Collapse;
const { Text, Title } = Typography

export class StakeholderDetailPlanning extends Component {

  componentDidMount() {
    const { actionGetDetail, match: { params } } = this.props
    return actionGetDetail(params.id)
  }
  
  render() {
    const { match: { params }, getData: { data, loading } } = this.props

    if(loading){
      return <Loading />
    }

    return (
      <React.Fragment>
        <PageHeader 
          className="site-page-header" 
          title="PERENCANAAN PROYEK" 
          ghost={true}
          onBack={() => this.props.history.push('/stakeholder/project')}
          extra={[
            <Menu key="1" selectedKeys="1" mode="horizontal" style={{ background: 'none' }}>
              <Menu.Item key="1" icon={<DatabaseOutlined />}>
                <Link to={`/stakeholder/project/planning/detail/${params.id}`}>
                  <Text>PERENCANAAN PROYEK</Text>
                </Link>
              </Menu.Item>
              <Menu.Item key="2" icon={<TableOutlined />}>
                <Link to={`/stakeholder/project/list/${params.id}`}>
                  <Text>TABEL PROYEK</Text>
                </Link>
              </Menu.Item>
            </Menu>,
          ]}
        />
        <Row gutter={16}>
          <Col span={24}>
            <Collapse defaultActiveKey={['1', '2', '3', '4']} span={24}>
              <Panel header="Informasi Perencanaan Proyek" key="1">
                <Row gutter={16}>
                  <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                    <Title level={5}>Nama Order:</Title>
                    <Text>{data.name ? data.name : 'N/A'}</Text>
                  </Col>
                  <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                    <Title level={5}>Nama Perusahaan:</Title>
                    <Text>{data.companyName ? data.companyName : 'N/A'}</Text>
                  </Col>
                  <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                    <Title level={5}>Jumlah Pesanan:</Title>
                    <Text>{data.total ? data.total : 'N/A'}</Text>
                  </Col>
                  <Col sm={24} md={12} style={{ marginBottom: 30 }}>
                    <Title level={5}>Status:</Title>
                    {
                      data.isHold ? 
                      <Text type="danger"> <Badge status="error" /> Tertunda </Text> 
                      : 
                      <Text type="warning"> <Badge status="warning" /> Sedang Berlangsung </Text> 
                    }
                  </Col>
                  <Col sm={24} md={24} style={{ marginBottom: 30 }}>
                    <Title level={5}>Keterangan:</Title>
                    <Text>{data.notes ? data.notes : 'N/A'}</Text>
                  </Col>
                </Row>
              </Panel>

              <Panel header="Jenis Ukuran" key="2">
                {data.size.length >= 0 ? data.size.map((item, i) => (
                  <Row gutter={16} key={i}>
                    <Col sm={24} md={12}>
                      <Title key={i} level={5}>Jenis Ukuran:</Title>
                      <Text>{item.typeSize}</Text>
                    </Col>
                    <Col sm={24} md={12}>
                      <Title key={i} level={5}>Proses:</Title>
                      {item.value && item.value.map((item, i) => (
                        <Tag key={i}>{item}</Tag> 
                      ))}
                    </Col>
                    <Divider />
                  </Row>
                )) : <Empty />
                }
              </Panel>
              <Panel header="Jenis Kemampuan" key="3">
                <Row gutter={16}>
                  <Col sm={24} md={24}>
                    <List
                      dataSource={data.skill}
                      loading={loading}
                      renderItem={(item, i) => (
                        <List.Item>
                          {`${i+1}. ${item}`}
                        </List.Item>
                      )}
                    />
                  </Col>
                </Row>
              </Panel>
              <Panel header="Komentar" key="4">
                <Row gutter={16}>
                  <Col sm={24} md={24}>
                    <List
                      className="comment-list"
                      itemLayout="horizontal"
                      dataSource={data.comment}
                      renderItem={item => (
                        <li>
                          <Comment
                            actions={[<Tooltip title="Stakeholder">{`Stakeholder: ${item.user.userContact.fullName}`}</Tooltip>]}
                            author={item.project.description.NAMA}
                            content={item.notes}
                            datetime={moment(item.created_at).format('LL')}
                          />
                        </li>
                      )}
                    />
                  </Col>
                </Row>
              </Panel>
            </Collapse>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetDetail } = this.props
    return unmountGetDetail()
  }
}

const mapStateToProps = (state) => ({
  getData: state.planning.detail
})

const mapDispatchToProps = {
  actionGetDetail   : detailPlanning,
  unmountGetDetail  : unmountDetailPlanning
}

export default connect(mapStateToProps, mapDispatchToProps)(StakeholderDetailPlanning)
