import React, { Component } from 'react'
import { columns } from './columns'
import { connect } from 'react-redux'
import { FilterTable } from '../../../components'
import { PageHeader, Row, Col, Table } from 'antd'
import { listPlanning, unmountDataPlanning } from '../../../redux/actions/planning/planningAction'

const qs = require('qs')

export class StakeholderProject extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      current: 1,
      perPage: 10,
      filter: {
        search: null,
        type: 'name',
        orderBy: 'desc'
      }
    }
  }

  componentDidMount() {
    const { actionGetData, location: { search } } = this.props
    const current = qs.parse(search, { ignoreQueryPrefix: true })
    const filter = {
      "type": current.name,
      "search": null,
      "orderBy": "desc"
    }
    if(current.page){
      this.setState({ current: current.page }, () => {
        return actionGetData(current.page, this.state.perPage, filter)
      })
    }else{
      return actionGetData(1, this.state.perPage, filter)
    }
  }

  pagination = (e) => {
    const { history, actionGetData } = this.props
    this.setState({ current: e.current, perPage: e.pageSize }, () => {
      actionGetData(e.current, e.pageSize, this.state.filter);
      return history.push({
        search: `page=${e.current}`
      })
    })
  }

  onSearch = (e) => {
    const { history, actionGetData } = this.props
    return this.setState({ "current": 1, "filter": e  }, () => {
      history.push()
      window.scrollTo(0, 0);
      return actionGetData(this.state.current, this.state.perPage, this.state.filter);
    })
  }
  
  render() {
    const { current, perPage } = this.state
    const { getData: { data, loading, pagination } } = this.props

    const defaultStatusFilter = [
      { value: 'name', name: 'Nama Order' },
    ]

    return (
      <React.Fragment>
        <PageHeader className="site-page-header" title="PROJECT" />
        <Row gutter={16}>
          <Col sm={24} md={24} style={{ marginBottom: 30 }}>
            <FilterTable 
              fase1={null}
              fase2={{ key: 'type', value: defaultStatusFilter}}
              onSearch={this.onSearch}
            />
          </Col>
          <Col sm={24} md={24}>
            <Table 
              rowKey={(i) => i.uid}
              loading={loading} 
              dataSource={data} 
              columns={columns(current, perPage)} 
              pagination={pagination} 
              onChange={this.pagination} 
            />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
  componentWillUnmount(){
    const { unmountGetData } = this.props
    return unmountGetData()
  }
}

const mapStateToProps = (state) => ({
  getData : state.planning.list
})

const mapDispatchToProps = {
  actionGetData   : listPlanning,
  unmountGetData  : unmountDataPlanning
}

export default connect(mapStateToProps, mapDispatchToProps)(StakeholderProject)
