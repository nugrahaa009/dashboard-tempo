import React from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import Route from './router';
import store from './redux';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import moment from 'moment';
import 'moment/locale/id'
moment.locale('id');

ReactDOM.render(
  <Provider store={store}>
    <Route />
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
